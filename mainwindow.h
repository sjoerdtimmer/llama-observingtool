#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "scan.h"
#include "valuewithunits.h"

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTranslator>
#include <QNetworkAccessManager>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    // override closeEvent to show a confirmation first
    void closeEvent (QCloseEvent *);

    // logging functions
    void LOG(const std::string&);
    void LOG(const char*);
    void LOG(const QString&);

    void ERR(const std::string&);
    void ERR(const char*);
    void ERR(const QString&);


    void setScan(Scan *);
//    void clearGui();
    void setGuiEnabled(bool);


private slots:
    void on_radioButton_listOfCoordinates_toggled(bool checked);

    void on_radioButton_rectangularGrid_toggled(bool checked);

    void on_comboBox_spectralType_currentIndexChanged(int index);

    void on_radioButton_OnTheFly_toggled(bool checked);

    void on_pushButton_removeCoordinateRow_clicked();

    void on_pushButton_addCoordinateRow_clicked();

    void on_pushButton_addScan_clicked();

    void on_pushButton_removeScan_clicked();

    void on_listWidget_scans_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_lineEdit_scanName_textChanged(const QString &arg1);

    void on_plainTextEdit_Description_textChanged();

    void on_lineEdit_sourceZ_textChanged(const QString &arg1);

    void on_pushButton_cloneScan_clicked();

    void on_actionEnglish_toggled(bool arg1);

    void on_actionDutch_toggled(bool arg1);

    void on_actionSpanish_toggled(bool arg1);

    void on_actionQuit_triggered();

    void on_actionCheck_for_updates_triggered();

    void on_comboBox_solarSystemObject_currentIndexChanged(const QString &arg1);

    void on_comboBox_solarSystemObject_activated(int index);

    void on_comboBox_sourceSystem_activated(int index);

    void on_radioButton_sourceFromTable_toggled(bool checked);

    void on_radioButton_sourceFromSolarSystem_toggled(bool checked);

    void on_radioButton_sourceFromEphemeris_toggled(bool checked);

    void on_radioButton_sourceFromCatalog_toggled(bool checked);

    void on_radioButton_hexGrid_toggled(bool checked);

    void on_radioButton_otfZigZag_toggled(bool checked);

    void on_radioButton_otfSameDir_toggled(bool checked);

    void on_spinBox_otfLineCount_valueChanged(int arg1);

    void on_spinBox_otfRepetitions_valueChanged(int arg1);

    void on_comboBox_spectralType_activated(int index);

    void on_comboBox_polarizationProduct_activated(int index);

    void on_comboBox_receiverBand_activated(int index);

    void on_comboBox_dopplerType_activated(int index);

    void on_comboBox_velocityReferenceFrame_activated(int index);

    void on_actionSave_triggered();

    void on_actionSave_as_triggered();

private:
    Ui::MainWindow *ui;
    Scan * currentScan = 0;
    Scan * emptyScan = 0;

    QTranslator qtLangEN;
    QTranslator qtLangNL;
    QTranslator qtLangES;

    QTranslator appLangEN;
    QTranslator appLangNL;
    QTranslator appLangES;

    QString m_fileName = "";

    QNetworkAccessManager dlManager;

//    ValueWithUnits * sourceDec;

    void loadTranslations();
    void saveToFile(QString filename);

};

#endif // MAINWINDOW_H
