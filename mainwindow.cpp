#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "scan.h"
#include "valuewithunits.h"

// qt includes
#include <QtGlobal>
#include <QGroupBox>
#include <QToolBox>
#include <QPlainTextEdit>
#include <QHeaderView>
#include <QTableWidget>
#include <QItemSelectionModel>
#include <QMessageBox>
#include <QList>
#include <QLibraryInfo>
#include <QCloseEvent>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QComboBox>
#include <QLineEdit>
#include <QDesktopServices>
#include <QSpinBox>
#include <QRadioButton>
#include <QSplitter>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include <QUuid>

// std includes
#include <iostream>
#include <sstream>
#include <ostream>
#include <set>
#include <unordered_set>


using std::cerr;
using std::cout;
using std::endl;




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    loadTranslations();

    // some gui components will only because visible upon selection the appropriate type of scan
    ui->groupCoordinates->hide();
    ui->groupGridOptions->hide();
    ui->groupOTF->hide();
    ui->groupBox_spectralContinuum->hide();
    ui->groupBox_spectralLine->hide();
    ui->groupBox_spectralScan->hide();

    // hide the log
    QList<int> sizes;
    sizes.append(9999);
    sizes.append(0);
    ui->splitter_2->setSizes(sizes);

    // because the things above take up space the gui editor has calculated a relatively large basesize.
    // the following looks better
    this->resize(1000,650);

    // a global empty scan object is used to reset the gui when no scan is selected
    this->emptyScan = new Scan("");

    // change appearance and behaviour of table element
    QHeaderView* header = ui->tableWidget_patternOffsets->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::Stretch);

    // init number without units
    ui->lineEdit_sourceZ->setValidator(new MyDoubleValidator());

    // init spinboxes:
    // nothing needs to be done

    // populate comboboxes:
    ui->comboBox_sourceSystem->clear();
    ui->comboBox_sourceSystem->addItem(tr("ICRS"),QVariant::fromValue(Scan::CoordinateSystemICRS));
    ui->comboBox_sourceSystem->addItem(tr("FK5"),QVariant::fromValue(Scan::CoordinateSystemFK5));
    ui->comboBox_sourceSystem->addItem(tr("Gallactic"),QVariant::fromValue(Scan::CoordinateSystemGallactic));
    ui->comboBox_sourceSystem->addItem(tr("Ecliptic"),QVariant::fromValue(Scan::CoordinateSystemEcliptic));
    ui->comboBox_sourceSystem->addItem(tr("Horizon"),QVariant::fromValue(Scan::CoordinateSystemHorizon));
    ui->comboBox_sourceSystem->addItem(tr("AzEl"),QVariant::fromValue(Scan::CoordinateSystemAzEl));

    ui->comboBox_solarSystemObject->clear();
    ui->comboBox_solarSystemObject->addItem(tr("Mercury"),QVariant::fromValue(Scan::Mercury));
    ui->comboBox_solarSystemObject->addItem(tr("Venus"),QVariant::fromValue(Scan::Venus));
    ui->comboBox_solarSystemObject->addItem(tr("Moon"),QVariant::fromValue(Scan::Moon));
    ui->comboBox_solarSystemObject->addItem(tr("Mars"),QVariant::fromValue(Scan::Mars));
    ui->comboBox_solarSystemObject->addItem(tr("Jupiter"),QVariant::fromValue(Scan::Jupiter));
    ui->comboBox_solarSystemObject->addItem(tr("Saturn"),QVariant::fromValue(Scan::Saturn));
    ui->comboBox_solarSystemObject->addItem(tr("Uranus"),QVariant::fromValue(Scan::Uranus));
    ui->comboBox_solarSystemObject->addItem(tr("Neptune"),QVariant::fromValue(Scan::Neptune));
    ui->comboBox_solarSystemObject->addItem(tr("Pluto"),QVariant::fromValue(Scan::Pluto));
    ui->comboBox_solarSystemObject->addItem(tr("Sun"),QVariant::fromValue(Scan::Sun));
    ui->comboBox_solarSystemObject->addItem(tr("Ganymede"),QVariant::fromValue(Scan::Ganymede));
    ui->comboBox_solarSystemObject->addItem(tr("Europa"),QVariant::fromValue(Scan::Europa));
    ui->comboBox_solarSystemObject->addItem(tr("Callisto"),QVariant::fromValue(Scan::Callisto));
    ui->comboBox_solarSystemObject->addItem(tr("Io"),QVariant::fromValue(Scan::Io));
    ui->comboBox_solarSystemObject->addItem(tr("Titan"),QVariant::fromValue(Scan::Titan));
    ui->comboBox_solarSystemObject->addItem(tr("Ceres"),QVariant::fromValue(Scan::Ceres));
    ui->comboBox_solarSystemObject->addItem(tr("Pallas"),QVariant::fromValue(Scan::Pallas));
    ui->comboBox_solarSystemObject->addItem(tr("Juno"),QVariant::fromValue(Scan::Juno));
    ui->comboBox_solarSystemObject->addItem(tr("Vesta"),QVariant::fromValue(Scan::Vesta));

    ui->comboBox_velocityReferenceFrame->clear();
    ui->comboBox_velocityReferenceFrame->addItem(tr("bar"),QVariant::fromValue(Scan::VelocityReferenceFrameBar));
    ui->comboBox_velocityReferenceFrame->addItem(tr("lsrk"),QVariant::fromValue(Scan::VelocityReferenceFrameLsrk));
    ui->comboBox_velocityReferenceFrame->addItem(tr("topo"),QVariant::fromValue(Scan::VelocityReferenceFrameTopo));
    ui->comboBox_velocityReferenceFrame->addItem(tr("hel"),QVariant::fromValue(Scan::VelocityReferenceFrameHel));

    ui->comboBox_spectralType->clear();
    ui->comboBox_spectralType->addItem(tr("single line"),QVariant::fromValue(Scan::SpectralTypeLine));
    ui->comboBox_spectralType->addItem(tr("continuum"),QVariant::fromValue(Scan::SpectralTypeContinuum));
    ui->comboBox_spectralType->addItem(tr("frequency scan"),QVariant::fromValue(Scan::SpectralTypeScan));
    
    ui->comboBox_polarizationProduct->clear();
    ui->comboBox_polarizationProduct->addItem(tr("dual"),QVariant::fromValue(Scan::SpectralPolarizationDual));
    ui->comboBox_polarizationProduct->addItem(tr("XX"),QVariant::fromValue(Scan::SpectralPolarizationXX));
    ui->comboBox_polarizationProduct->addItem(tr("full"),QVariant::fromValue(Scan::SpectralPolarizationFull));

    ui->comboBox_receiverBand->clear();
    ui->comboBox_receiverBand->addItem(tr("band 3"), QVariant::fromValue(Scan::Band3));
    ui->comboBox_receiverBand->addItem(tr("band 4"), QVariant::fromValue(Scan::Band4));
    ui->comboBox_receiverBand->addItem(tr("band 5"), QVariant::fromValue(Scan::Band5));
    ui->comboBox_receiverBand->addItem(tr("band 6"), QVariant::fromValue(Scan::Band6));
    ui->comboBox_receiverBand->addItem(tr("band 7"), QVariant::fromValue(Scan::Band7));
    ui->comboBox_receiverBand->addItem(tr("band 8"), QVariant::fromValue(Scan::Band8));
    ui->comboBox_receiverBand->addItem(tr("band 9"), QVariant::fromValue(Scan::Band9));
    ui->comboBox_receiverBand->addItem(tr("band 10"), QVariant::fromValue(Scan::Band10));

    ui->comboBox_dopplerType->clear();
    ui->comboBox_dopplerType->addItem(tr("radio"), QVariant::fromValue(Scan:: DopplerTypeRadio));
    ui->comboBox_dopplerType->addItem(tr("optical"), QVariant::fromValue(Scan:: DopplerTypeOptical));
    ui->comboBox_dopplerType->addItem(tr("relativistic"), QVariant::fromValue(Scan:: DopplerTypeRelativistic));

    // already create the first scan so that the interface becomes active
    on_pushButton_addScan_clicked();

}


MainWindow::~MainWindow()
{
    delete ui;
}

// methods for handling error and log messages

void MainWindow::LOG(const std::string &msg)
{
    QString s = QString::fromStdString(msg);
    ui->plainTextLog->appendPlainText(QString("LOG: ")+s);
}
void MainWindow::LOG(const char *msg)
{
    QString s = QString(msg);
    ui->plainTextLog->appendPlainText(QString("LOG: ")+s);
}
void MainWindow::LOG(const QString &msg)
{
    ui->plainTextLog->appendPlainText(QString("LOG: ")+msg);
}
void MainWindow::ERR(const std::string &msg)
{
    QString s = QString::fromStdString(msg);
    ui->plainTextLog->appendPlainText(QString("ERROR: ")+s);
}
void MainWindow::ERR(const char *msg)
{
    QString s = QString(msg);
    ui->plainTextLog->appendPlainText(QString("ERROR: ")+s);
}
void MainWindow::ERR(const QString &msg)
{
    ui->plainTextLog->appendPlainText(QString("ERROR: ")+msg);
}



void MainWindow::on_radioButton_listOfCoordinates_toggled(bool checked)
{
    if (checked)
    {
        currentScan->setPatternType(Scan::PatternTypeList);
        ui->groupCoordinates->show();
    }
    else
    {
        ui->groupCoordinates->hide();
    }
}

void MainWindow::on_radioButton_rectangularGrid_toggled(bool checked)
{
    if (checked)
    {
        currentScan->setPatternType(Scan::PatternTypeRectangular);
        ui->groupGridOptions->show();
    }
    else
    {
        ui->groupGridOptions->hide();
    }
}

void MainWindow::on_radioButton_OnTheFly_toggled(bool checked)
{
    if (checked)
    {
        currentScan->setPatternType(Scan::PatternTypeOTF);
        ui->groupOTF->show();
    }
    else
    {
        ui->groupOTF->hide();
    }
}

void MainWindow::on_comboBox_spectralType_currentIndexChanged(int index)
{
    ui->groupBox_spectralContinuum->hide();
    ui->groupBox_spectralLine->hide();
    ui->groupBox_spectralScan->hide();
    switch (index)
    {
    case 0: // spectral line
        ui->groupBox_spectralLine->show();
        break;
    case 1: // spectral continuum
        ui->groupBox_spectralContinuum->show();
        break;
    case 2: // spectral scan
        ui->groupBox_spectralScan->show();
        break;
    }
}

void MainWindow::on_pushButton_removeCoordinateRow_clicked()
{
    QItemSelectionModel * m = ui->tableWidget_patternOffsets->selectionModel();
    QModelIndexList list = m->selectedIndexes();
    // both cells may be selected at once resulting in a duplicate removal. to prevent this we use a set
    std::set<int> rownumbers;
    for (QModelIndex item : list)
    {
        rownumbers.insert(item.row());
    }
    // by going from back to front the later items don't change index when removing earlier items (set is ordered)
    for (std::set<int>::reverse_iterator it = rownumbers.rbegin(); it != rownumbers.rend(); ++it)
    {
        LOG(tr("removing row %1").arg(*it));
        ui->tableWidget_patternOffsets->removeRow(*it);
    }
}

void MainWindow::on_pushButton_addCoordinateRow_clicked()
{
    ui->tableWidget_patternOffsets->insertRow(ui->tableWidget_patternOffsets->rowCount());
}

void MainWindow::on_pushButton_addScan_clicked()
{
    static int counter = 1;
    Scan * newscan = new Scan(tr("new scan %1").arg(counter++), ui->listWidget_scans);
    ui->listWidget_scans->setCurrentItem(newscan); // this will trigger a selection change event
}

void MainWindow::on_pushButton_removeScan_clicked()
{
    QListWidgetItem * item = ui->listWidget_scans->currentItem();
    if (!item)
    {
        ERR(tr("no item selected to remove!"));
        return;
    }
    if (item->type() != Scan::ScanType)
    {
        ERR(tr("scan item object not of Scan type!"));
        return;
    }
    QMessageBox msgBox;
    msgBox.setText(tr("Are you sure?"));
    msgBox.setInformativeText(tr("This currently selected scan will be deleted and all info will be lost. This cannot be undone."));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    if ( QMessageBox::Yes == msgBox.exec() )
    {
        Scan * scan = (Scan*) item;
        delete scan;
        // the destructor of the QListWidgetItem will notify the ListWidget
        // and a selection change event will be fired and handled before the
        // actual deletion takes place.
    }
    // ListWidget will automatically select another item or clear selection if this was the last
}

void MainWindow::on_pushButton_cloneScan_clicked()
{
    QListWidgetItem * item = ui->listWidget_scans->currentItem();
    if (!item) return; // empty selection
    if (item->type() != Scan::ScanType)
    {
        ERR(tr("scan object not of type Scan!!"));
        return;
    }
    Scan * oldscan = (Scan *)item;
//    Scan * newscan = new Scan(*oldscan, ui->listWidget_scans);
    Scan * newscan = oldscan->clone();
    ui->listWidget_scans->setCurrentItem(newscan);
}


void MainWindow::on_listWidget_scans_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if (previous && current)
    {
        LOG(tr("switching from %1 to %2").arg(previous->text()).arg(current->text()));
    }
    else if (previous)
    {
        LOG(tr("switching from %1 to nothing").arg(previous->text()));
    }
    else if (current)
    {
        LOG(tr("switching from nothing to %1").arg(current->text()));
    }

    if (current)
    {
        // first a safety check
        if (current->type() != Scan::ScanType) ERR("Listitem that should be of type Scan is not of type Scan");
        // enable gui elements that may have been disabled after removing the last scan
        setGuiEnabled(true);
        ui->pushButton_cloneScan->setEnabled(true);
        ui->pushButton_removeScan->setEnabled(true);
        // copy scan text to gui
        setScan((Scan*)current);
    }
    else
    {
        // nothing was selected, disable gui parts that require a selected scan
        setGuiEnabled(false);
        ui->pushButton_cloneScan->setEnabled(false);
        ui->pushButton_removeScan->setEnabled(false);

        // by setting the gui to the empty scan, all fields are cleared.
        // this is not strictly necessary but it looks nice
        setScan(emptyScan);
    }
}



void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "LLama Observing Tool",
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}

void MainWindow::setScan(Scan * scan)
{

    // disconnect previous scan signals
    if (currentScan)
    {
        currentScan->getSourceRA()->disconnectGui();
        currentScan->getSourceDec()->disconnectGui();
        currentScan->getRadialVelocity()->disconnectGui();
        currentScan->getParallax()->disconnectGui();
        currentScan->getPmRA()->disconnectGui();
        currentScan->getPmDec()->disconnectGui();
        currentScan->getPatternRAOffset()->disconnectGui();
        currentScan->getPatternDecOffset()->disconnectGui();
        currentScan->getPointSpacing()->disconnectGui();
        currentScan->getPatternAngle()->disconnectGui();
        currentScan->getPLength()->disconnectGui();
        currentScan->getQLength()->disconnectGui();
        currentScan->getOtfAmplitude()->disconnectGui();
        currentScan->getOtfDuration()->disconnectGui();
        currentScan->getOtfLineOffset()->disconnectGui();
        currentScan->getOtfAngle()->disconnectGui();
        currentScan->getSpectralLineRepresentativeFreq()->disconnectGui();
        currentScan->getSkyFrequency()->disconnectGui();
        currentScan->getRestFrequency()->disconnectGui();
        currentScan->getSpectralContinuumRepresentativeFreq()->disconnectGui();
    }

    // connect VAlueWithUnits elements to gui, this immediately updates their shown values
    scan->getSourceRA()->connectGui(ui->lineEdit_sourceRA, ui->comboBox_sourceRAUnits);
    scan->getSourceDec()->connectGui(ui->lineEdit_sourceDec, ui->comboBox_sourceDecUnits);
    scan->getRadialVelocity()->connectGui(ui->lineEdit_radialVelocity, ui->comboBox_radialVelocityUnits);
    scan->getParallax()->connectGui(ui->lineEdit_sourceParallax, ui->comboBox_parallaxUnits);
    scan->getPmRA()->connectGui(ui->lineEdit_pmRA, ui->comboBox_pmra_units);
    scan->getPmDec()->connectGui(ui->lineEdit_pmDec, ui->comboBox_pmdec_units);
    scan->getPatternRAOffset()->connectGui(ui->lineEdit_patternRAOffset, ui->comboBox_patternRAOffsetUnits);
    scan->getPatternDecOffset()->connectGui(ui->lineEdit_patternDecOffset, ui->comboBox_patternDecOffsetUnits);
    scan->getPointSpacing()->connectGui(ui->lineEdit_patternSpacing, ui->comboBox_patternSpacingUnits);
    scan->getPatternAngle()->connectGui(ui->lineEdit_patternAngle, ui->comboBox_patternAngleUnits);
    scan->getPLength()->connectGui(ui->lineEdit_patternPLength, ui->comboBox_patternPUnits);
    scan->getQLength()->connectGui(ui->lineEdit_patternQLength, ui->comboBox_patternQUnits);
    scan->getOtfAmplitude()->connectGui(ui->lineEdit_otfAmplitude, ui->comboBox_otfAmplitudeUnits);
    scan->getOtfDuration()->connectGui(ui->lineEdit_otfDuration, ui->comboBox_otfDurationUnits);
    scan->getOtfLineOffset()->connectGui(ui->lineEdit_otfOffset, ui->comboBox_otfOffsetUnits);
    scan->getOtfAngle()->connectGui(ui->lineEdit_otfAngle, ui->comboBox_otfAngleUnits);
    scan->getSpectralLineRepresentativeFreq()->connectGui(ui->lineEdit_lineFrequency, ui->comboBox_lineFrequencyUnits);
    scan->getSkyFrequency()->connectGui(ui->lineEdit_skyFrequency, ui->comboBox_skyFrequencyUnits);
    scan->getRestFrequency()->connectGui(ui->lineEdit_restFrequency, ui->comboBox_restFrequencyUnits);
    scan->getSpectralContinuumRepresentativeFreq()->connectGui(ui->lineEdit_representativeFreq, ui->comboBox_representativeFreqUnits);

    // keep a pointer to the new scan in the mainwindow class
    currentScan = scan;

    // update the text fields:
    ui->lineEdit_scanName->setText(scan->getName());
    ui->plainTextEdit_Description->setPlainText(scan->getDescription());
    ui->lineEdit_sourceZ->setText(QString("%1").arg(scan->getZ(),0,'f'));

    // update the spinboxes
    ui->spinBox_otfLineCount->setValue(scan->getOtfLines());
    ui->spinBox_otfRepetitions->setValue(scan->getOtfRepetitions());

    // update comboboxes:
    int index;
    index = ui->comboBox_sourceSystem->findData(scan->getCoordinateSystem());
    if ( index > -1 ) 
    { 
        ui->comboBox_sourceSystem->setCurrentIndex(index);
    }
    index = ui->comboBox_solarSystemObject->findData(scan->getSolarSystemObject());
    if ( index > -1 ) 
    { 
        ui->comboBox_solarSystemObject->setCurrentIndex(index);
    }
    index = ui->comboBox_velocityReferenceFrame->findData(scan->getVelocityReferenceFrame());
    if ( index > -1 )
    {
        ui->comboBox_velocityReferenceFrame->setCurrentIndex(index);
    }
    index = ui->comboBox_dopplerType->findData(scan->getDopplerType());
    if ( index > -1 )
    {
        ui->comboBox_dopplerType->setCurrentIndex(index);
    }
    index = ui->comboBox_spectralType->findData(scan->getSpectralType());
    if ( index > -1 )
    {
        ui->comboBox_spectralType->setCurrentIndex(index);
    }
    index = ui->comboBox_polarizationProduct->findData(scan->getSpectralPolarization());
    if ( index > -1 )
    {
        ui->comboBox_polarizationProduct->setCurrentIndex(index);
    }
    index = ui->comboBox_receiverBand->findData(scan->getSpectralBand());
    if ( index > -1 )
    {
        ui->comboBox_receiverBand->setCurrentIndex(index);
    }

    // update radio items:
    switch (scan->getSourceType())
    {
        case Scan::SourceTypeManual:      ui->radioButton_sourceFromTable->setChecked(true); break;
        case Scan::SourceTypeSolarSystem: ui->radioButton_sourceFromSolarSystem->setChecked(true); break;
        case Scan::SourceTypeEphemeris:   ui->radioButton_sourceFromEphemeris->setChecked(true); break;
        case Scan::SourceTypeCatalog:     ui->radioButton_sourceFromCatalog->setChecked(true); break;
    }
    switch (scan->getPatternType())
    {
        case Scan::PatternTypeList:        ui->radioButton_listOfCoordinates->setChecked(true); break;
        case Scan::PatternTypeRectangular: ui->radioButton_rectangularGrid->setChecked(true); break;
        case Scan::PatternTypeOTF:         ui->radioButton_OnTheFly->setChecked(true); break;
        case Scan::PatternTypeHexagonal:   ui->radioButton_hexGrid->setChecked(true); break;
    }
    switch(scan->getOtfType())
    {
        case Scan::OTFTypeZigZag:  ui->radioButton_otfZigZag->setChecked(true); break;
        case Scan::OTFTypeSameDir: ui->radioButton_otfSameDir->setChecked(true); break;
    }

}

void MainWindow::setGuiEnabled(bool enabled)
{
    ui->tabScanDesc->setEnabled(enabled);
    ui->tabScanSource->setEnabled(enabled);
    ui->tabScanPattern->setEnabled(enabled);
    ui->tabScanSpectral->setEnabled(enabled);
    ui->tabScanSummary->setEnabled(enabled);
}

// Handlers for ui components that are not linked to a ValueWithUnits object:
void MainWindow::on_lineEdit_scanName_textChanged(const QString &arg1)
{
    currentScan->setName(arg1);
}
void MainWindow::on_plainTextEdit_Description_textChanged()
{
    currentScan->setDescription(ui->plainTextEdit_Description->toPlainText());
}
void MainWindow::on_lineEdit_sourceZ_textChanged(const QString &arg1)
{
    currentScan->setZ(arg1.toDouble());
}
void MainWindow::on_comboBox_solarSystemObject_activated(int index)
{
    QVariant var = ui->comboBox_solarSystemObject->itemData(index);
    if (var.canConvert<Scan::SolarSystemObject>())
    {
        currentScan->setSolarSystemObject(var.value<Scan::SolarSystemObject>());
    }
}
void MainWindow::on_comboBox_sourceSystem_activated(int index)
{
    QVariant var = ui->comboBox_sourceSystem->itemData(index);
    if (var.canConvert<Scan::CoordinateSystem>())
        currentScan->setCoordinateSystem(var.value<Scan::CoordinateSystem>());
}
void MainWindow::on_comboBox_velocityReferenceFrame_activated(int index)
{
    QVariant var = ui->comboBox_velocityReferenceFrame->itemData(index);
    if (var.canConvert<Scan::VelocityReferenceFrame>())
        currentScan->setVelocityReferenceFrame(var.value<Scan::VelocityReferenceFrame>());
}
void MainWindow::on_comboBox_dopplerType_activated(int index)
{
    QVariant var = ui->comboBox_dopplerType->itemData(index);
    if (var.canConvert<Scan::DopplerType>())
        currentScan->setDopplerType(var.value<Scan::DopplerType>());
}
void MainWindow::on_comboBox_spectralType_activated(int index)
{
    QVariant var = ui->comboBox_spectralType->itemData(index);
    if (var.canConvert<Scan::SpectralType>())
    {
        currentScan->setSpectralType(var.value<Scan::SpectralType>());
    }
}
void MainWindow::on_comboBox_polarizationProduct_activated(int index)
{
    QVariant var = ui->comboBox_polarizationProduct->itemData(index);
    if (var.canConvert<Scan::SpectralPolarization>())
    {
        currentScan->setSpectralPolarization(var.value<Scan::SpectralPolarization>());
    }
}
void MainWindow::on_comboBox_receiverBand_activated(int index)
{
    QVariant var = ui->comboBox_receiverBand->itemData(index);
    if (var.canConvert<Scan::Band>())
    {
        currentScan->setSpectralBand(var.value<Scan::Band>());
    }
}
void MainWindow::on_radioButton_sourceFromTable_toggled(bool checked)
{
    if(checked) currentScan->setSourceType(Scan::SourceTypeManual);
}
void MainWindow::on_radioButton_sourceFromSolarSystem_toggled(bool checked)
{
    if(checked) currentScan->setSourceType(Scan::SourceTypeSolarSystem);
}
void MainWindow::on_radioButton_sourceFromEphemeris_toggled(bool checked)
{
    if(checked) currentScan->setSourceType(Scan::SourceTypeEphemeris);
}
void MainWindow::on_radioButton_sourceFromCatalog_toggled(bool checked)
{
    if(checked) currentScan->setSourceType(Scan::SourceTypeCatalog);
}
void MainWindow::on_radioButton_hexGrid_toggled(bool checked)
{
    if(checked) currentScan->setPatternType(Scan::PatternTypeHexagonal);
}
void MainWindow::on_radioButton_otfZigZag_toggled(bool checked)
{
    if(checked) currentScan->setOtfType(Scan::OTFTypeZigZag);
}
void MainWindow::on_radioButton_otfSameDir_toggled(bool checked)
{
    if(checked) currentScan->setOtfType(Scan::OTFTypeSameDir);
}
void MainWindow::on_spinBox_otfLineCount_valueChanged(int arg1)
{
    currentScan->setOtfLines(arg1);
}
void MainWindow::on_spinBox_otfRepetitions_valueChanged(int arg1)
{
    currentScan->setOtfRepetitions(arg1);
}


void MainWindow::loadTranslations()
{
    if (qtLangEN.load("qt_en", QLibraryInfo::location(QLibraryInfo::TranslationsPath)))   LOG(tr("qt translations for english successfully loaded"));
    else ERR(tr("english qt translations not found"));
    if (qtLangNL.load("qt_nl", QLibraryInfo::location(QLibraryInfo::TranslationsPath)))   LOG(tr("qt translations for dutch successfully loaded"));
    else ERR(tr("dutch qt translations not found"));
    if (qtLangES.load("qt_es", QLibraryInfo::location(QLibraryInfo::TranslationsPath)))   LOG(tr("qt translations for spanish successfully loaded"));
    else ERR(tr("spanish qt translations not found"));

    if (appLangEN.load(":translations/i18n/llamaobstool_en_US"))   LOG(tr("app translations for english successfully loaded"));
    else ERR(tr("english app translations not found"));
    if (appLangNL.load(":translations/i18n/llamaobstool_nl_NL"))   LOG(tr("app translations for dutch successfully loaded"));
    else ERR(tr("dutch app translations not found"));
    if (appLangES.load(":translations/i18n/llamaobstool_es_CL"))   LOG(tr("app translations for spanish successfully loaded"));
    else ERR(tr("spanish app translations not found"));

    ui->actionEnglish->setChecked(true);

    QActionGroup * languageGroup = new QActionGroup(this);
    languageGroup->addAction(ui->actionDutch);
    languageGroup->addAction(ui->actionEnglish);
    languageGroup->addAction(ui->actionSpanish);

    ui->actionEnglish->setChecked(true);
}

void MainWindow::on_actionEnglish_toggled(bool checked)
{
    if (checked)
    {
        LOG(tr("%1 enabled").arg("en_US"));
        QApplication::instance()->installTranslator(&qtLangEN);
        QApplication::instance()->installTranslator(&appLangEN);
    }
    else
    {
        LOG(tr("%1 disabled").arg("en_US"));
        QApplication::instance()->removeTranslator(&qtLangEN);
        QApplication::instance()->removeTranslator(&appLangEN);
    }
    ui->retranslateUi(this);
}

void MainWindow::on_actionDutch_toggled(bool checked)
{
    if (checked)
    {
        LOG(tr("%1 enabled").arg("nl_NL"));
        QApplication::instance()->installTranslator(&qtLangNL);
        QApplication::instance()->installTranslator(&appLangNL);
    }
    else
    {
        LOG(tr("%1 disabled").arg("nl_NL"));
        QApplication::instance()->removeTranslator(&qtLangNL);
        QApplication::instance()->removeTranslator(&appLangNL);
    }
    ui->retranslateUi(this);
}

void MainWindow::on_actionSpanish_toggled(bool checked)
{
    if (checked)
    {
        LOG(tr("%1 enabled").arg("es_CL"));
        QApplication::instance()->installTranslator(&qtLangES);
        QApplication::instance()->installTranslator(&appLangES);
    }
    else
    {
        LOG(tr("%1 disabled").arg("es_CL"));
        QApplication::instance()->removeTranslator(&qtLangES);
        QApplication::instance()->removeTranslator(&appLangES);
    }
    ui->retranslateUi(this);
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_actionCheck_for_updates_triggered()
{
    // prep request
    QUrl url("https://bitbucket.org/sjoerdtimmer/llama-observingtool/downloads/CURRENT_VERSION");
    QNetworkRequest request(url);

#if QT_VERSION > QT_VERSION_CHECK(5,9,0)    
    // there is two ways to enable auto-follow-redirect: neither works with qt < 5.9
    // request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, QVariant(true));
     dlManager.setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
#endif

    // request a reply
    dlManager.get(request);

    // beware, anonyous function syntax ahead. This is c++11, and only supported by qt 5.2 and up (see https://wiki.qt.io/New_Signal_Slot_Syntax)
    connect(&dlManager, &QNetworkAccessManager::finished, [=](QNetworkReply * r) {
        QString data = QString(r->readAll());
        QStringList versionnumbers = data.split("\\.");
        if (versionnumbers.length() < 2)
        {
            LOG(QString("Version number '%1' does not contain at least one dot(.)").arg(data));
            return;
        }
        int major = versionnumbers[0].toInt();
        int minor = versionnumbers[1].toInt();

        LOG(QString("latest version is %1.%2").arg(major).arg(minor));
        LOG(QString("current version is %1.%2").arg(VERSION_MAJOR).arg(VERSION_MINOR));

        if (major == VERSION_MAJOR && minor == VERSION_MINOR)
        {
            QMessageBox msgBox;
            msgBox.setText("Your software appears to be up to date.");
            msgBox.exec();
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText(QString("Your software (version %1.%2) does not match the lateest release version %3.%4").arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(major).arg(minor));
            msgBox.setInformativeText("Would you like to go to the download page now?");
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::Yes);
            if (msgBox.exec() == QMessageBox::Yes)
            {
                if ( ! QDesktopServices::openUrl(QUrl("https://bitbucket.org/sjoerdtimmer/llama-observingtool/downloads")))
                {
                    LOG("opening url failed!");
                }
            }
        }
    });
}

void MainWindow::on_comboBox_solarSystemObject_currentIndexChanged(const QString &arg1)
{
    LOG(tr("item changed to '%1'").arg(arg1));
}




void MainWindow::on_actionSave_triggered()
{
    // trigger a file dialog if no file is selected
    if (m_fileName == "")
    {
        // save after showing a dialog
        on_actionSave_as_triggered();
    }
    else
    {
        // save the the previously selected file
        saveToFile(m_fileName);
    }


}

void MainWindow::on_actionSave_as_triggered()
{
    QStringList locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    QString location = "";
    if (locations.size()>0) location = locations[0]; // HomeLocation guarantees at least one result, but better safe than sorry

    QFileDialog fileDialog(this);
    fileDialog.setNameFilter("Observation files (*.xml)");
    fileDialog.setDirectory(location);
    fileDialog.setDefaultSuffix(".xml");
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    if (!fileDialog.exec())
    {   // save cancelled
        return;
    }
    fileDialog.close();
    m_fileName = fileDialog.selectedFiles().at(0);
    saveToFile(m_fileName);
}



void MainWindow::saveToFile(QString filename)
{
    cout << "saving to \"" << filename.toStdString() << "\"" << endl;

    QFile file(filename);
    if (! file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(0,"error",file.errorString());
        return;
    }
    QXmlStreamWriter xml(&file);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();


    xml.writeStartElement("PartialSchedBlock");
    xml.writeAttribute("xmlns", "LLama/ObsPrep/SchedBlock");
    xml.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
    xml.writeAttribute("schemaVersion", "0.0");
    for (int i=0 ; i < ui->listWidget_scans->count() ; ++i)
    {
        QListWidgetItem * item = ui->listWidget_scans->item(i);
        if (!item) return; // empty selection
        if (item->type() != Scan::ScanType)
        {
            ERR(tr("scan object not of type Scan!!"));
            return;
        }
        Scan * scan = (Scan*)item;
        xml.writeStartElement("FieldSource");
        xml.writeAttribute("entityPartId", scan->getUUID().toString());
        xml.writeStartElement("scanName");
        xml.writeCharacters(scan->getName());
        xml.writeEndElement(); // scanName
        xml.writeStartElement("scanType");
        xml.writeCharacters(XMLStrings::sourceTypeNames.value(scan->getSourceType()));
        xml.writeEndElement(); // scanType
        xml.writeStartElement("scanDescription");
        xml.writeCharacters(scan->getDescription());
        xml.writeEndElement(); // scanDescription
        xml.writeStartElement("scanSource");
        xml.writeStartElement("longitude");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSourceRA()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSourceRA()->getValue(),0,'f',10));
        xml.writeEndElement(); // longitude
        xml.writeStartElement("latitude");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSourceDec()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSourceDec()->getValue(),0,'f',10));
        xml.writeEndElement(); // latitude
        xml.writeStartElement("solarSystemObject");
        xml.writeCharacters(XMLStrings::solarSystemObjectNames.value(scan->getSolarSystemObject()));
        xml.writeEndElement(); // solarSystemObject
        xml.writeEndElement(); // scanSource
        xml.writeStartElement("sourceVelocity");
        xml.writeAttribute("referenceSystem", XMLStrings::velocityReferenceFrameNames.value(scan->getVelocityReferenceFrame()));
        xml.writeStartElement("centerVelocity");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getRadialVelocity()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getRadialVelocity()->getValue(),0,'f',10));
        xml.writeEndElement(); // centerVelocity
        xml.writeEndElement(); // sourcevelocity
        xml.writeStartElement("sourceDopplerZ");
        xml.writeAttribute("type", XMLStrings::dopplerTypeNames.value(scan->getDopplerType()));
        xml.writeCharacters(QString("%1").arg(scan->getZ(),0,'f',10));
        xml.writeEndElement(); // sourceDopplerZ
        xml.writeStartElement("pMRA");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPmRA()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPmRA()->getValue(),0,'f',10));
        xml.writeEndElement(); // pMRA
        xml.writeStartElement("pMDec");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPmDec()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPmDec()->getValue(),0,'f',10));
        xml.writeEndElement(); // pMDec
        xml.writeStartElement("parallax");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getParallax()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getParallax()->getValue(),0,'f',10));
        xml.writeEndElement(); // parallax
        xml.writeStartElement("sourceEphemeris");
        xml.writeEndElement(); // sourceEphemeris

        xml.writeStartElement("sourcePattern");
        xml.writeAttribute("type", XMLStrings::patternTypeNames.value(scan->getPatternType()));
        xml.writeAttribute("scanDirection", "");
        xml.writeStartElement("longitudeLength");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPatternRAOffset()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPatternRAOffset()->getValue(),0,'f',10));
        xml.writeEndElement(); // longitudeLength
        xml.writeStartElement("latitudeLength");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPatternDecOffset()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPatternDecOffset()->getValue(),0,'f',10));
        xml.writeEndElement(); // latitudeLength
        xml.writeStartElement("pointSpacing");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPointSpacing()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPointSpacing()->getValue(),0,'f',10));
        xml.writeEndElement(); // pointSpacing
        xml.writeStartElement("orientation");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPatternAngle()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPatternAngle()->getValue(),0,'f',10));
        xml.writeEndElement(); // orientation
        xml.writeStartElement("orthogonalLength");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getPLength()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getPLength()->getValue(),0,'f',10));
        xml.writeEndElement(); // orthogonalLength
        xml.writeStartElement("parallelLength");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getQLength()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getQLength()->getValue(),0,'f',10));
        xml.writeEndElement(); // parallelLength
        xml.writeStartElement("scanVelocity");
        xml.writeAttribute("unit", "");
        xml.writeEndElement(); // scanVelocity
        xml.writeStartElement("coordinateList");
        xml.writeAttribute("unit", "");
        xml.writeEndElement(); // coordinateList
        xml.writeStartElement("otfType");
        xml.writeCharacters(XMLStrings::otfTypeNames.value(scan->getOtfType()));
        xml.writeEndElement(); // otfType
        xml.writeStartElement("otfAmplitude");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getOtfAmplitude()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getOtfAmplitude()->getValue(),0,'f',10));
        xml.writeEndElement(); // otfAmplitude
        xml.writeStartElement("otfDuration");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getOtfDuration()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getOtfDuration()->getValue(),0,'f',10));
        xml.writeEndElement(); // otfDuration
        xml.writeStartElement("otfLineOffset");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getOtfLineOffset()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getOtfLineOffset()->getValue(),0,'f',10));
        xml.writeEndElement(); // otfLineOffset
        xml.writeStartElement("otfOrientation");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getOtfAngle()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getOtfAngle()->getValue(),0,'f',10));
        xml.writeEndElement(); // otfOrientation
        xml.writeStartElement("otfRepetitions");
        xml.writeCharacters(QString("%1").arg(scan->getOtfRepetitions()));
        xml.writeEndElement(); // otfRepetitions
        xml.writeStartElement("otfLines");
        xml.writeCharacters(QString("%1").arg(scan->getOtfLines()));
        xml.writeEndElement(); // otfLines
        xml.writeEndElement(); // sourcePattern

        xml.writeStartElement("sourceSpectral");
        xml.writeAttribute("type", XMLStrings::spectralTypeNames.value(scan->getSpectralType()));
        xml.writeAttribute("polarizationProduct", XMLStrings::spectralPolarizationNames.value(scan->getSpectralPolarization()));
        xml.writeStartElement("band");
        xml.writeCharacters(XMLStrings::bandNames.value(scan->getSpectralBand()));
        xml.writeEndElement();  // band
        xml.writeStartElement("representativeFrequency");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSpectralLineRepresentativeFreq()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSpectralLineRepresentativeFreq()->getValue()));
        xml.writeEndElement(); // representativeFrequency
        xml.writeStartElement("skyFrequency");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSkyFrequency()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSkyFrequency()->getValue()));
        xml.writeEndElement(); // skyFrequency
        xml.writeStartElement("restFrequency");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getRestFrequency()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getRestFrequency()->getValue()));
        xml.writeEndElement(); // restFrequency
        xml.writeStartElement("continuumRepresentativeFrequency");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSpectralContinuumRepresentativeFreq()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSpectralContinuumRepresentativeFreq()->getValue()));
        xml.writeEndElement(); // continuumRepresentativeFrequency
        xml.writeStartElement("frequencyScanStart");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSpectralScanStartFreq()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSpectralScanStartFreq()->getValue()));
        xml.writeEndElement(); // frequencyScanStart
        xml.writeStartElement("frequencyScanEnd");
        xml.writeAttribute("unit", Units::xmlNames.value(scan->getSpectralScanEndFreq()->getUnits()));
        xml.writeCharacters(QString("%1").arg(scan->getSpectralScanEndFreq()->getValue()));
        xml.writeEndElement(); // frequencyScanEnd
        xml.writeEndElement(); // sourceSpectral
        xml.writeEndElement(); // fieldSource
    }


    xml.writeEndDocument();
    file.close();
}

