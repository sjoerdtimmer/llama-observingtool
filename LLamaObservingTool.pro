#-------------------------------------------------
#
# Project created by QtCreator 2017-10-23T11:30:24
#
#-------------------------------------------------

QT       += core gui network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LLamaObservingTool
TEMPLATE = app

RC_FILE = LLamaObservingTool.rc

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# define version number:
VERSION_MAJOR = 0
VERSION_MINOR = 1
# make version number available through C preprocessor macros
DEFINES +=  "VERSION_MAJOR=$$VERSION_MAJOR"\
            "VERSION_MINOR=$$VERSION_MINOR"

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        scan.cpp \
        valuewithunits.cpp \
    units.cpp

HEADERS += \
        mainwindow.h \
        scan.h \
        units.h \
        valuewithunits.h

FORMS += \
        mainwindow.ui

TRANSLATIONS    = i18n/llamaobstool_en_US.ts \
                  i18n/llamaobstool_nl_NL.ts \
                  i18n/llamaobstool_pt_BR.ts \
                  i18n/llamaobstool_es_CL.ts

RESOURCES += \
    translations.qrc \
    images.qrc

DISTFILES += \
    LLamaObservingTool.rc
