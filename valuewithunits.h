#ifndef QVALUEWITHUNITS_H
#define QVALUEWITHUNITS_H

#include <QObject>
#include <QLineEdit>
#include <QComboBox>
#include <QVariant>





/* Note:
  It would be "cleaner" to use template classes here. It is possible to define the c++ class
  in such a way that it takes an argument that defines the "type" of units (i.e. lengths, frequencies, angles etc.)
  However, Q_OBJECT does not support template classes so we resort to a generic unit represented by a QVariant.
  The correctness is only checked at runtime, not at compile time.
*/

class ValueWithUnits : public QObject
{
    Q_OBJECT
public:
    explicit ValueWithUnits(QObject *parent = nullptr);
//    ValueWithUnits(const ValueWithUnits&, QObject *parent = nullptr);
    ValueWithUnits(ValueWithUnits* other, QObject *parent = nullptr);
    ValueWithUnits(QVariant &unit, double value, QObject *parent = nullptr);

    void setAllowedUnits(QVariantList *units);
    QVariantList * getAllowedUnits();

//    QString getText();
    double getValue();
    QVariant getUnits();
    void connectGui(QLineEdit *valueEdit, QComboBox *unitEdit);
    void disconnectGui();

signals:
//    void valueEdited(double);
//    void valueChanged(double);
//    void numberEdited(QString&);
//    void numberChanged(QString&);
//    void unitsEdited(QVariant&);
//    void unitsChanged(QVariant&);

public slots:
    void setValue(const QString&);
    void setValue(double);
    void setUnits(int);
    void setUnits(const QVariant&);

private:
    double parseNumber(const QString&, const QVariant units=QVariant());
    QString formatNumber(double, const QVariant units=QVariant());

    void updateUI();

    double unitConversion(const QVariant &oldunit, const QVariant &newunit);

    bool checkType(QVariant&);
    bool checkTypes(QVariantList *variantlist);

    QLineEdit * m_valueEdit = 0;
    QComboBox * m_unitEdit = 0;

    QVariant m_units;
    double m_value;
    QVariantList * m_allowedUnits = 0;

};

//Q_DECLARE_METATYPE(ValueWithUnits)

#endif // QVALUEWITHUNITS_H
