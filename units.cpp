#include "units.h"

#include <QString>
#include <QRegExp>
#include <QStringBuilder>

#include <iostream>

using std::cout;
using std::endl;
using std::cerr;


uint qHash( const QVariant & var )
{
    if ( !var.isValid() || var.isNull() )
        //return -1;
        Q_ASSERT(0);

    if ( var.type()==QVariant::UserType )
    {
        return 1000*var.userType() + var.value<int>();
    }

    // could not generate a hash for the given variant
    return -1;
}



QValidator::State HelloWorldValidator::validate(QString &input, int &pos) const {
    cout << "validating \'" << input.toStdString() << "\' at position " << pos << endl;
    if (input == "Hello World!") return QValidator::Acceptable;
    if (QString("Hello World!").startsWith(input))
    {
        return QValidator::Intermediate;
    }
    return QValidator::Invalid;
}
void  HelloWorldValidator::fixup(QString &input) const {
    cout << "requested fixup of \'" << input.toStdString() << "\'" << endl;
    input = "Hello World!";
}


QValidator::State MyDoubleValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos);
    QRegExp reIntermediate("^[\\-]?\\d*(\\.\\d*)?$");
    QRegExp reAcceptable("^[\\-]?\\d+\\.\\d+$");
    if (reAcceptable.exactMatch(input))
    {
        return QValidator::Acceptable;
    }
    if (reIntermediate.exactMatch(input))
    {
        return QValidator::Intermediate;
    }
    return QValidator::Invalid;
}
void MyDoubleValidator::fixup(QString &input) const
{
    cout << "fixing up a double" << endl;
    QRegExp rx("^([\\-]?)(\\d*)(\\.(\\d*))?$");
    rx.exactMatch(input);
    QString sign = rx.cap(1);
    QString decimal = rx.cap(2);
    QString fractional = rx.cap(3);
    if (decimal == "") decimal = "0";
    if (!fractional.contains(".")) fractional.prepend('.');
    if (fractional.length()==1) fractional.append("0");
    input = sign % decimal % fractional;
//    for (int i=0;i<rx.captureCount();i++)
//    {
//        cout << "capture " << i << ": " << rx.cap(i).toStdString() << endl;
//    }
}


QValidator::State AngleHoursValidator::validate(QString &input, int &pos) const
{
//    cout << "validating \'" << input.toStdString() << "\' at pos " << pos << endl;

    // catch the special case where someone removed everything:
    if (input == "")
    {
        input = "00:00:00.00000";
        return QValidator::Acceptable;
    }

    // this is a first filter that prevents insertion of bad characters (a-z, symbols and +/- anywhere after the start.
    // it also prevents the removal of the : and . separators
    QRegExp rx("^([\\-]?)(\\d*):(\\d*):(\\d*)\\.(\\d*)");
    if (!rx.exactMatch(input))
    {
//        cout << "bad format" << endl;
        return QValidator::Invalid;
    }

    // split the input string:
    QString sign    = rx.cap(1);
    QString hours   = rx.cap(2);
    QString minutes = rx.cap(3);
    QString seconds = rx.cap(4);
    QString subseconds = rx.cap(5);

    // fix the fixed length part of the input:
    // determine the string that contains only the digits,
    // and the position within this string (i.e. number of digits left of cursor)
    QString digits = hours+minutes+seconds;
    int posindigits = std::min(6,input.left(pos).count(QRegExp("\\d")));

    // now correct for extra or missing digits:
    while (digits.length() < 6)
    {   // add 0's right of cursor:
        digits.insert(posindigits, QChar('0'));
    }
    if (digits.length() > 6)
    {
        // remove digits right of cursor:
        digits.remove(posindigits, digits.length()-6);
    }

    // fix the floating part of the number:
    if (subseconds == "" )
        subseconds = "0";


    // determine cursor shift:
    if (pos <= input.indexOf('.'))
    {   // the edit occured in the left part, put cursor back left
        if (posindigits >= 6)     pos = sign.length() + 9;               // put it after the separating .
        else if (posindigits > 4) pos = sign.length() + posindigits + 2; // put it back at original position including 2 :'s
        else if (posindigits > 2) pos = sign.length() + posindigits + 1; // put it back at original position including 1 :
        else                      pos = sign.length() + posindigits;     // put it back at original position (no :'s)
    }
    else
    {   // editing was done on floating part
        pos = 8 + (pos - input.indexOf('.')); // keep original offset from .
    }

    // rebuild string: (% is overloaded for QStrings to do efficient concatenation)
    input = sign % digits[0] % digits[1] % ":" % digits[2] % digits[3] % ":" % digits[4] % digits[5] % "." % subseconds;


    return QValidator::Acceptable;
}

void  AngleHoursValidator::fixup(QString &input) const {
    Q_UNUSED(input);
    // this is never called because the validate immediately corrects any mistakes
    // I.e. if validate returns Invalid the keystrokes are simply rejected
    // and when validate return Acceptable no fixup is deemed necessary
}



AngleHoursValidator * getAngleHourValidatorInstance()
{
    static AngleHoursValidator * _instance = 0;
    if (_instance == 0 )
        _instance = new AngleHoursValidator();
    return _instance;
}
MyDoubleValidator * getDoubleValidatorInstance()
{
    static MyDoubleValidator * _instance = 0;
    if (_instance == 0 )
        _instance = new MyDoubleValidator();
    return _instance;
}
