#include "valuewithunits.h"
#include "scan.h"

#include <QComboBox>
#include <QVariantList>

#include <iostream>
#include <typeinfo>
#include <cmath>

using std::cout;
using std::cerr;
using std::endl;



ValueWithUnits::ValueWithUnits(QObject *parent) : QObject(parent)
{
    cout << "warning: default constructor of ValueWithUnits not properly tested yet!" << endl;
}

ValueWithUnits::ValueWithUnits(ValueWithUnits *other, QObject *parent) : QObject(parent)
{
    m_units = other->getUnits();
    m_value = other->getValue();
    setAllowedUnits(other->getAllowedUnits());
}

ValueWithUnits::ValueWithUnits(QVariant &unit, double value, QObject *parent) : QObject(parent)
{
    m_units = unit;
    m_value = value;
}


void ValueWithUnits::setAllowedUnits(QVariantList *units)
{
    // sanity checks:
    checkTypes(units);
    if (units->size()==0)
    {
        cerr << "at least one unit must be supported!" << endl;
    }

    // copy the list for good measure:
    m_allowedUnits = new QVariantList(*units);
}

QVariantList * ValueWithUnits::getAllowedUnits()
{
    return m_allowedUnits;
}


void ValueWithUnits::connectGui(QLineEdit *valueEdit, QComboBox *unitEdit)
{
    cout << "gui connections rebuilt" << endl;
    // save pointers:
    m_unitEdit = unitEdit;
    m_valueEdit = valueEdit;

    // populate combobox (possibly duplicate):
    m_unitEdit->clear();
    for (auto it = m_allowedUnits->begin(); it != m_allowedUnits->end(); ++it)
    {
        QVariant var = QVariant::fromValue(*it);
        m_unitEdit->addItem(Units::names[var], var);

//        cout << "lookup of unit type " << var.userType() << " resulted in " << Units::names.value(var, tr("\"not found\"")).toStdString() << endl;
    }

    // copy current values to ui:
    setValue(m_value);
    setUnits(m_units);
    // setting the units will automatically update the rendered text

    // connect signal to combobox:
    // the activated signal is chosen because it doesn't trigger when the values of the ui elements change programatically.
    // two static_casts are required here because both signal and slot are overloaded and the compiler can't tell which signature is needed
    connect(unitEdit, static_cast<void(QComboBox::*)(int)>(&QComboBox::activated), this, static_cast<void(ValueWithUnits::*)(int)>(&ValueWithUnits::setUnits));

    // connect signal to linedit:
    // the textEdited signal would be more appropriate here since it only triggers on 'human edits' but it does not trigger
    // if the 'human edit' is modified by the validator. Hence we have to use ::textChanged and be careful not to get into
    // signal loops when programatically setting the text. I.e., don't call setText from anything that can be reached from
    // within this signal handler: (changing the units, which is also done at init or at scan change triggers a programatic
    // textedit, but this doesn't create a cycle)
    connect(valueEdit, &QLineEdit::textChanged, this, static_cast<void(ValueWithUnits::*)(const QString&)>(&ValueWithUnits::setValue));

}
void ValueWithUnits::disconnectGui()
{
    disconnect(m_valueEdit , 0, this, 0);
    disconnect(m_unitEdit  , 0, this, 0);
    m_valueEdit = 0;
    m_unitEdit = 0;
}

void ValueWithUnits::updateUI()
{
    if (m_valueEdit)
    {
        // set the new validator:
        if (m_units == Units::Hours)
            m_valueEdit->setValidator(getAngleHourValidatorInstance());
        else
            m_valueEdit->setValidator(getDoubleValidatorInstance());
        // set the new text
        m_valueEdit->setText(formatNumber(m_value, m_units));
    }
    if (m_unitEdit && m_allowedUnits)
    {
        int unit_index =  m_allowedUnits->lastIndexOf(m_units);
        if (unit_index < 0)
        {
            cout << "current unit is not one of the allowed units?" << endl;
        }
        else
        {
            m_unitEdit->setCurrentIndex(unit_index);
        }
    }
}

void ValueWithUnits::setValue(double newvalue)
{
    m_value = newvalue;
}
void ValueWithUnits::setUnits(const QVariant &newunit)
{
    m_value *= unitConversion(m_units, newunit);
    m_units = newunit;
    updateUI();
}
// alternative signatures for the above two functions:
void ValueWithUnits::setValue(const QString &val)
{
    setValue(parseNumber(val, m_units));
}
void ValueWithUnits::setUnits(int index)
{
    setUnits(m_unitEdit->itemData(index));
}



double ValueWithUnits::getValue()
{
    return m_value;
}
QVariant ValueWithUnits::getUnits()
{
    return m_units;
}



double ValueWithUnits::parseNumber(const QString& number, const QVariant units)
{
    QVariant hours = QVariant::fromValue(Units::Hours);
    if (units.userType() == hours.userType() && units == hours)
    {
//        cout << "parsing hours " << endl;
        QRegExp rx("([\\-]?)(\\d*):(\\d*):(\\d+\\.\\d+)");

        if(rx.exactMatch(number))
        {
            int sign = (rx.cap(1)+"1").toInt();
            int hours = rx.cap(2).toInt();
            int minutes = rx.cap(3).toInt();
            double seconds = rx.cap(4).toDouble();
            return (double)sign * ((double)hours + (double)minutes/60.0 + seconds / 3600.0);
        }
        else
        {
//            cout << "regex did not match" << endl;
            return 0.0;
        }

    }
    else
    {
//        cout << "parsing number" << endl;
        bool ok;
        double d;
        if (number.endsWith('.'))
        {
            QString tmp = number.left(number.length()-1);
            d = tmp.toDouble(&ok);
        }
        else
        {
            d = number.toDouble(&ok);
        }
        return ok?d:0.0;
    }
}

QString ValueWithUnits::formatNumber(double d, const QVariant units)
{
    if (units.userType() == QVariant::fromValue(Units::Hours).userType() && units.value<Units::Angle>()==Units::Hours)
    {
        int hours   = floor(d);
        int mins    = floor((d-hours)*60.0);
        double secs = (d-(double)hours-(double)mins/60.0)*3600.0;

        return QString("%1:%2:%3")
            .arg(hours, 2, 10, QLatin1Char('0'))
            .arg(mins,  2, 10, QLatin1Char('0'))
            .arg(secs,  8, 'f', 5, QLatin1Char('0'));
    }
    else
    {
        return QString("%1").arg(d,0,'f',9);
    }
}


bool ValueWithUnits::checkType(QVariant &var)
{
    return ( var.userType() == QVariant::fromValue(Units::Radians).userType()
          || var.userType() == QVariant::fromValue(Units::MetersPerSec).userType()
          || var.userType() == QVariant::fromValue(Units::Minutes).userType()
          || var.userType() == QVariant::fromValue(Units::GHz).userType()
          || var.userType() == QVariant::fromValue(Units::ArcMinPerSec).userType() );
}

bool ValueWithUnits::checkTypes(QVariantList *variantlist)
{
    // check that all knows types:
    for (auto it = variantlist->begin(); it != variantlist->end(); ++it)
    {
        if (!checkType(*it))
        {
            cerr << "Unit " << QVariant::typeToName(it->userType()) << " is not one of the types that ValueWithUnits understands!!!!" << endl;
        }
    }

    // check that all the same
    if (variantlist->size() < 2) return true;
    for (auto it = ++(variantlist->begin()); it != variantlist->end(); ++it)
    {
        if (it->userType() != variantlist->at(0).userType())
        {
            cerr << "all units for one combobox must be of the same type!!!!" << endl;
            return false;
        }
    }
    return true;
}


// the following would not be necessary if we were to subclass a variant for each possible type
double ValueWithUnits::unitConversion(const QVariant &oldunit, const QVariant &newunit)
{
    return Units::intrinsicvalues[oldunit] / Units::intrinsicvalues[newunit];
}
