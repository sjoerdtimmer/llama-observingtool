#include "scan.h"

#include <QString>
#include <QFile>

#include <iostream>
#include <cmath>

using std::cout;
using std::endl;
using std::cerr;



Scan::Scan(QString name, QListWidget *list)
    :QListWidgetItem(name, list, ScanType)
    ,m_uuid(QUuid::createUuid())
    ,m_name(name)
{
    // initialize members (not the ValueWithUnit ones, that's done below)
    m_description = "";
    m_sourceType = Scan::SourceTypeManual;
    m_solarSystemObject = Scan::Pluto;
    m_coordinateSystem = Scan::CoordinateSystemFK5;
    m_velocityReferenceFrame = Scan::VelocityReferenceFrameBar;
    m_z = 0.0;
    m_dopplerType = Scan::DopplerTypeRadio;
    m_patternType = Scan::PatternTypeRectangular;
    m_otfType = Scan::OTFTypeZigZag;
    m_otfLines = 10;
    m_otfRepetitions = 1;
    m_spectralType = Scan::SpectralTypeLine;
    m_spectralPolarization = Scan::SpectralPolarizationDual;
    m_spectralBand = Scan::Band3;
    

    // declare some shorthands for units:
    QVariant degrees   = QVariant::fromValue(Units::Degrees);
    QVariant hours     = QVariant::fromValue(Units::Hours);
    QVariant arcsec    = QVariant::fromValue(Units::ArcSec);
    QVariant mas       = QVariant::fromValue(Units::MilliArcSec);
    QVariant seconds   = QVariant::fromValue(Units::Seconds);
    QVariant degpersec = QVariant::fromValue(Units::DegreesPerSec);
    QVariant masperyear= QVariant::fromValue(Units::MASPerYear);
    QVariant meterpers = QVariant::fromValue(Units::MetersPerSec);
    QVariant ghz       = QVariant::fromValue(Units::GHz);

    // initialize default values:
    m_sourceRA                            = new ValueWithUnits(hours,            0.0, this);
    m_sourceDec                           = new ValueWithUnits(degrees,          0.0, this);
    m_radialVelocity                      = new ValueWithUnits(meterpers,        0.0, this);
    m_parallax                            = new ValueWithUnits(mas,              0.0, this);
    m_pmRA                                = new ValueWithUnits(masperyear,       0.0, this);
    m_pmDec                               = new ValueWithUnits(masperyear,       0.0, this);
    m_patternRAOffset                     = new ValueWithUnits(mas,              0.0, this);
    m_patternDecOffset                    = new ValueWithUnits(mas,              0.0, this);
    m_pointSpacing                        = new ValueWithUnits(mas,              0.0, this);
    m_patternAngle                        = new ValueWithUnits(degrees,          0.0, this);
    m_pLength                             = new ValueWithUnits(mas,              0.0, this);
    m_qLength                             = new ValueWithUnits(mas,              0.0, this);
    m_otfAmplitude                        = new ValueWithUnits(mas,              0.0, this);
    m_otfDuration                         = new ValueWithUnits(seconds,          0.0, this);
    m_otfLineOffset                       = new ValueWithUnits(mas,              0.0, this);
    m_otfAngle                            = new ValueWithUnits(degrees,          0.0, this);
    m_spectralLineRepresentativeFreq      = new ValueWithUnits(ghz,              0.0, this);
    m_skyFrequency                        = new ValueWithUnits(ghz,              0.0, this);
    m_restFrequency                       = new ValueWithUnits(ghz,              0.0, this);
    m_spectralContinuumRepresentativeFreq = new ValueWithUnits(ghz,              0.0, this);
    m_spectralScanStartFreq               = new ValueWithUnits(ghz,              0.0, this);
    m_spectralScanEndFreq                 = new ValueWithUnits(ghz,              0.0, this);

    // set allowed units:
    QVariantList * commonCoordUnits = new QVariantList();
    commonCoordUnits->append(QVariant::fromValue(Units::Degrees));
    commonCoordUnits->append(QVariant::fromValue(Units::Hours));
    commonCoordUnits->append(QVariant::fromValue(Units::Radians));
    
    QVariantList * commonFreqUnits = new QVariantList();
    commonFreqUnits->append(QVariant::fromValue(Units::Hz));
    commonFreqUnits->append(QVariant::fromValue(Units::kHz));
    commonFreqUnits->append(QVariant::fromValue(Units::MHz));
    commonFreqUnits->append(QVariant::fromValue(Units::GHz));

    QVariantList * commonPatternUnits = new QVariantList();
    commonPatternUnits->append(QVariant::fromValue(Units::MilliArcSec));
    commonPatternUnits->append(QVariant::fromValue(Units::ArcSec));
    commonPatternUnits->append(QVariant::fromValue(Units::ArcMin));
    commonPatternUnits->append(QVariant::fromValue(Units::Degrees));
    commonPatternUnits->append(QVariant::fromValue(Units::Radians));

    QVariantList * radialVelocityUnits = new QVariantList();
    radialVelocityUnits->append(QVariant::fromValue(Units::MetersPerSec));
    radialVelocityUnits->append(QVariant::fromValue(Units::KilometersPerSec));

    QVariantList * commonAngleUnits = new QVariantList();
    commonAngleUnits->append(QVariant::fromValue(Units::Degrees));
    commonAngleUnits->append(QVariant::fromValue(Units::Radians));

    QVariantList * durationUnits = new QVariantList();
    durationUnits->append(QVariant::fromValue(Units::Seconds));
    durationUnits->append(QVariant::fromValue(Units::Minutes));

    QVariantList * properMotionUnits = new QVariantList();
    properMotionUnits->append(QVariant::fromValue(Units::MASPerYear));
    properMotionUnits->append(QVariant::fromValue(Units::DegreesPerSec));
    properMotionUnits->append(QVariant::fromValue(Units::ArcMinPerSec));
    properMotionUnits->append(QVariant::fromValue(Units::ArcSecPerSec));

    m_sourceRA->setAllowedUnits(commonCoordUnits);
    m_sourceDec->setAllowedUnits(commonCoordUnits);
    m_patternRAOffset->setAllowedUnits(commonCoordUnits);
    m_patternDecOffset->setAllowedUnits(commonCoordUnits);
    
    m_radialVelocity->setAllowedUnits(radialVelocityUnits);

    m_patternAngle->setAllowedUnits(commonAngleUnits);
    m_otfAngle->setAllowedUnits(commonAngleUnits);

    m_pmRA->setAllowedUnits(properMotionUnits);
    m_pmDec->setAllowedUnits(properMotionUnits);

    m_otfDuration->setAllowedUnits(durationUnits);
    
    m_parallax->setAllowedUnits(commonPatternUnits);
    m_pointSpacing->setAllowedUnits(commonPatternUnits);
    m_pLength->setAllowedUnits(commonPatternUnits);
    m_qLength->setAllowedUnits(commonPatternUnits);
    m_otfAmplitude->setAllowedUnits(commonPatternUnits);
    m_otfLineOffset->setAllowedUnits(commonPatternUnits);
    
    m_skyFrequency->setAllowedUnits(commonFreqUnits);
    m_restFrequency->setAllowedUnits(commonFreqUnits);
    m_spectralLineRepresentativeFreq->setAllowedUnits(commonFreqUnits);
    m_spectralContinuumRepresentativeFreq->setAllowedUnits(commonFreqUnits);
    m_spectralScanStartFreq->setAllowedUnits(commonFreqUnits);
    m_spectralScanEndFreq->setAllowedUnits(commonFreqUnits);
}


Scan * Scan::clone()
{
    Scan * res = new Scan(QString("copy of ")+this->getName(), this->listWidget());
    // copy "regular" members
    res->setDescription(getDescription());
    res->setSourceType(getSourceType());
    res->setSolarSystemObject(getSolarSystemObject());
    res->setCoordinateSystem(getCoordinateSystem());
    res->setVelocityReferenceFrame(getVelocityReferenceFrame());
    res->setZ(getZ());
    res->setDopplerType(getDopplerType());
    res->setPatternType(getPatternType());
    res->setOtfType(getOtfType());
    res->setOtfLines(getOtfLines());
    res->setOtfRepetitions(getOtfRepetitions());
    res->setSpectralType(getSpectralType());
    res->setSpectralPolarization(getSpectralPolarization());
    res->setSpectralBand(getSpectralBand());

    // copy valuewithunits members
    res->setSourceRA(new ValueWithUnits(getSourceRA()));
    res->setSourceDec(new ValueWithUnits(getSourceDec()));
    res->setRadialVelocity(new ValueWithUnits(getRadialVelocity()));
    res->setParallax(new ValueWithUnits(getParallax()));
    res->setPmRA(new ValueWithUnits(getPmRA()));
    res->setPmDec(new ValueWithUnits(getPmDec()));
    res->setPatternRAOffset(new ValueWithUnits(getPatternRAOffset()));
    res->setPatternDecOffset(new ValueWithUnits(getPatternDecOffset()));
    res->setPointSpacing(new ValueWithUnits(getPointSpacing()));
    res->setPatternAngle(new ValueWithUnits(getPatternAngle()));
    res->setPLength(new ValueWithUnits(getPLength()));
    res->setQLength(new ValueWithUnits(getQLength()));
    res->setOtfAmplitude(new ValueWithUnits(getOtfAmplitude()));
    res->setOtfDuration(new ValueWithUnits(getOtfDuration()));
    res->setOtfLineOffset(new ValueWithUnits(getOtfLineOffset()));
    res->setOtfAngle(new ValueWithUnits(getOtfAngle()));
    res->setSpectralLineRepresentativeFreq(new ValueWithUnits(getSpectralLineRepresentativeFreq()));
    res->setSkyFrequency(new ValueWithUnits(getSkyFrequency()));
    res->setRestFrequency(new ValueWithUnits(getRestFrequency()));
    res->setSpectralContinuumRepresentativeFreq(new ValueWithUnits(getSpectralContinuumRepresentativeFreq()));
    res->setSpectralScanStartFreq(new ValueWithUnits(getSpectralScanStartFreq()));
    res->setSpectralScanEndFreq(new ValueWithUnits(getSpectralScanEndFreq()));
    return res;
}

Scan::~Scan()
{
    cout << "Scan destroyed" << endl;
}

QUuid                         Scan::getUUID()                                const {return m_uuid; }
QString                       Scan::getName()                                const { return m_name; }
QString                       Scan::getDescription()                         const { return m_description; }
Scan::SourceType              Scan::getSourceType()                          const { return m_sourceType; }
Scan::SolarSystemObject       Scan::getSolarSystemObject()                   const { return m_solarSystemObject; }
Scan::CoordinateSystem        Scan::getCoordinateSystem()                    const { return m_coordinateSystem; }
ValueWithUnits *              Scan::getSourceRA()                                  { return m_sourceRA; }
ValueWithUnits *              Scan::getSourceDec()                                 { return m_sourceDec; }
ValueWithUnits *              Scan::getRadialVelocity()                            { return m_radialVelocity; }
Scan::VelocityReferenceFrame  Scan::getVelocityReferenceFrame()              const { return m_velocityReferenceFrame; }
double                        Scan::getZ()                                   const { return m_z; }
Scan::DopplerType             Scan::getDopplerType()                         const { return m_dopplerType; }
ValueWithUnits *              Scan::getParallax()                                  { return m_parallax; }
ValueWithUnits *              Scan::getPmRA()                                      { return m_pmRA; }
ValueWithUnits *              Scan::getPmDec()                                     { return m_pmDec; }
Scan::PatternType             Scan::getPatternType()                         const { return m_patternType; }
Scan::OTFType                 Scan::getOtfType()                             const { return m_otfType; }
ValueWithUnits *              Scan::getPatternRAOffset()                           { return m_patternRAOffset; }
ValueWithUnits *              Scan::getPatternDecOffset()                          { return m_patternDecOffset; }
ValueWithUnits *              Scan::getPointSpacing()                              { return m_pointSpacing; }
ValueWithUnits *              Scan::getPatternAngle()                              { return m_patternAngle; }
ValueWithUnits *              Scan::getPLength()                                   { return m_pLength; }
ValueWithUnits *              Scan::getQLength()                                   { return m_qLength; }
ValueWithUnits *              Scan::getOtfAmplitude()                              { return m_otfAmplitude; }
ValueWithUnits *              Scan::getOtfDuration()                               { return m_otfDuration; }
ValueWithUnits *              Scan::getOtfLineOffset()                             { return m_otfLineOffset; }
ValueWithUnits *              Scan::getOtfAngle()                                  { return m_otfAngle; }
int                           Scan::getOtfLines()                            const { return m_otfLines; }
int                           Scan::getOtfRepetitions()                      const { return m_otfRepetitions; }
Scan::SpectralType            Scan::getSpectralType()                        const { return m_spectralType; }
Scan::SpectralPolarization    Scan::getSpectralPolarization()                const { return m_spectralPolarization; }
ValueWithUnits *              Scan::getSpectralLineRepresentativeFreq()            { return m_spectralLineRepresentativeFreq; }
Scan::Band                    Scan::getSpectralBand()                        const { return m_spectralBand; }
ValueWithUnits *              Scan::getSkyFrequency()                              { return m_skyFrequency; }
ValueWithUnits *              Scan::getRestFrequency()                             { return m_restFrequency; }
ValueWithUnits *              Scan::getSpectralContinuumRepresentativeFreq()       { return m_spectralContinuumRepresentativeFreq; }
ValueWithUnits *              Scan::getSpectralScanStartFreq()                     { return m_spectralScanStartFreq; }
ValueWithUnits *              Scan::getSpectralScanEndFreq()                       { return m_spectralScanEndFreq; }



//void nameChanged(const QString&);
//void descriptionChanged(const QString&);

// the first one is different because the "text" property belonging to the QListModelItem must also be set
void Scan::setName(const QString &name)
{
    m_name = name;
    setText(m_name);
}
// the rest are all the same/simple
void Scan::setDescription(const QString &description)                   { m_description = description;                  }
void Scan::setSourceType(const SourceType &type)                        { m_sourceType = type;                          }
void Scan::setSolarSystemObject(const SolarSystemObject &val)           { m_solarSystemObject = val;                    }
void Scan::setCoordinateSystem(const CoordinateSystem &val)             { m_coordinateSystem = val;                     }
void Scan::setSourceRA(ValueWithUnits * val)                            { m_sourceRA = val;                             }
void Scan::setSourceDec(ValueWithUnits * val)                           { m_sourceDec = val;                            }
void Scan::setRadialVelocity(ValueWithUnits * val)                      { m_radialVelocity = val;                       }
void Scan::setVelocityReferenceFrame(const VelocityReferenceFrame &val) { m_velocityReferenceFrame = val;               }
void Scan::setZ(const double &val)                                      { m_z = val;                                    }
void Scan::setDopplerType(const DopplerType &val)                       { m_dopplerType = val;                          }
void Scan::setParallax(ValueWithUnits * val)                            { m_parallax = val;                             }
void Scan::setPmRA(ValueWithUnits * val)                                { m_pmRA = val;                                 }
void Scan::setPmDec(ValueWithUnits * val)                               { m_pmDec = val;                                }
void Scan::setPatternType(const PatternType &val)                       { m_patternType = val;                          }
void Scan::setOtfType(const OTFType &val)                               { m_otfType = val;                              }
void Scan::setPatternRAOffset(ValueWithUnits * val)                     { m_patternRAOffset = val;                      }
void Scan::setPatternDecOffset(ValueWithUnits * val)                    { m_patternDecOffset = val;                     }
void Scan::setPointSpacing(ValueWithUnits * val)                        { m_pointSpacing = val;                         }
void Scan::setPatternAngle(ValueWithUnits * val)                        { m_patternAngle = val;                         }
void Scan::setPLength(ValueWithUnits * val)                             { m_pLength = val;                              }
void Scan::setQLength(ValueWithUnits * val)                             { m_qLength = val;                              }
void Scan::setOtfAmplitude(ValueWithUnits * val)                        { m_otfAmplitude = val;                         }
void Scan::setOtfDuration(ValueWithUnits * val)                         { m_otfDuration = val;                          }
void Scan::setOtfLineOffset(ValueWithUnits * val)                       { m_otfLineOffset = val;                        }
void Scan::setOtfAngle(ValueWithUnits * val)                            { m_otfAngle = val;                             }
void Scan::setOtfLines(const int &val)                                  { m_otfLines = val;                             }
void Scan::setOtfRepetitions(const int &val)                            { m_otfRepetitions = val;                       }
void Scan::setSpectralType(const SpectralType &val)                     { m_spectralType = val;                         }
void Scan::setSpectralPolarization(const SpectralPolarization &val)     { m_spectralPolarization = val;                 }
void Scan::setSpectralLineRepresentativeFreq(ValueWithUnits * val)      { m_spectralLineRepresentativeFreq = val;       }
void Scan::setSpectralBand(const Band &val)                             { m_spectralBand = val;                         }
void Scan::setSkyFrequency(ValueWithUnits * val)                        { m_skyFrequency = val;                         }
void Scan::setRestFrequency(ValueWithUnits * val)                       { m_restFrequency = val;                        }
void Scan::setSpectralContinuumRepresentativeFreq(ValueWithUnits * val) { m_spectralContinuumRepresentativeFreq = val;  }
void Scan::setSpectralScanStartFreq(ValueWithUnits * val)               { m_spectralScanStartFreq = val; }
void Scan::setSpectralScanEndFreq(ValueWithUnits * val)                 { m_spectralScanEndFreq = val; }

