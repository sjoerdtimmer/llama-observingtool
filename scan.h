#ifndef SCAN_H
#define SCAN_H

#include "units.h"
#include "valuewithunits.h"

#include <QObject>
#include <QWidget>
#include <QString>
#include <QListWidgetItem>
#include <QListWidget>
#include <QUuid>

class Scan : public QObject, public QListWidgetItem, public QVariant
{
    Q_OBJECT

    Q_ENUMS(SourceType)
    Q_ENUMS(CoordinateSystem)
    Q_ENUMS(VelocityReferenceFrame)
    Q_ENUMS(DopplerType)
    Q_ENUMS(PatternType)
    Q_ENUMS(OTFType)
    Q_ENUMS(SpectralType)
    Q_ENUMS(SpectralPolarization)
    Q_ENUMS(Band)
    Q_ENUMS(SolarSystemObject)
    




public:
    // constructors and destructors
    Scan(QString name, QListWidget *list = Q_NULLPTR);
    ~Scan();


    // enums
    enum SourceType {SourceTypeManual, SourceTypeSolarSystem, SourceTypeEphemeris, SourceTypeCatalog};
    enum CoordinateSystem {CoordinateSystemICRS, CoordinateSystemFK5, CoordinateSystemGallactic, CoordinateSystemEcliptic, CoordinateSystemHorizon, CoordinateSystemAzEl};
    enum VelocityReferenceFrame {VelocityReferenceFrameBar, VelocityReferenceFrameLsrk, VelocityReferenceFrameTopo,  VelocityReferenceFrameHel};
    enum DopplerType {DopplerTypeRadio, DopplerTypeOptical, DopplerTypeRelativistic};
    enum PatternType {PatternTypeList, PatternTypeRectangular, PatternTypeOTF, PatternTypeHexagonal};
    enum OTFType {OTFTypeZigZag, OTFTypeSameDir};
    enum SpectralType {SpectralTypeLine, SpectralTypeContinuum, SpectralTypeScan};
    enum SpectralPolarization {SpectralPolarizationDual, SpectralPolarizationXX, SpectralPolarizationFull};
    enum Band {Band3, Band4, Band5, Band6, Band7, Band8, Band9, Band10};
    enum SolarSystemObject {Mercury, Venus, Moon, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto, Sun, Ganymede, Europa, Callisto, Io, Titan, Ceres, Pallas, Juno, Vesta};





    // add a new QMetaType usertype:
    enum
    {
        UserType = QListWidgetItem::UserType,
        ScanType
    };

    // getters:
    QUuid                  getUUID()  const;
    QString                getName() const;
    QString                getDescription() const;
    SourceType             getSourceType() const;
    SolarSystemObject      getSolarSystemObject() const;
    CoordinateSystem       getCoordinateSystem() const;
    VelocityReferenceFrame getVelocityReferenceFrame() const;
    double                 getZ() const;
    DopplerType            getDopplerType() const;
    PatternType            getPatternType() const;
    OTFType                getOtfType() const;
    int                    getOtfLines() const;
    int                    getOtfRepetitions() const;
    SpectralType           getSpectralType() const;
    SpectralPolarization   getSpectralPolarization() const;
    Band                   getSpectralBand() const;

    // getters for stuff with units:
    ValueWithUnits *       getSourceRA();
    ValueWithUnits *       getSourceDec();
    ValueWithUnits *       getRadialVelocity();
    ValueWithUnits *       getParallax();
    ValueWithUnits *       getPmRA();
    ValueWithUnits *       getPmDec();
    ValueWithUnits *       getPatternRAOffset();
    ValueWithUnits *       getPatternDecOffset();
    ValueWithUnits *       getPointSpacing();
    ValueWithUnits *       getPatternAngle();
    ValueWithUnits *       getPLength();
    ValueWithUnits *       getQLength();
    ValueWithUnits *       getOtfAmplitude();
    ValueWithUnits *       getOtfDuration();
    ValueWithUnits *       getOtfLineOffset();
    ValueWithUnits *       getOtfAngle();
    ValueWithUnits *       getSpectralLineRepresentativeFreq();
    ValueWithUnits *       getSkyFrequency();
    ValueWithUnits *       getRestFrequency();
    ValueWithUnits *       getSpectralContinuumRepresentativeFreq();
    ValueWithUnits *       getSpectralScanStartFreq();
    ValueWithUnits *       getSpectralScanEndFreq();    

    // other public methods:
    Scan * clone();


public slots: 

    
    // setters
    void setName(const QString&);
    void setDescription(const QString&);
    void setSourceType(const SourceType&);
    void setSolarSystemObject(const SolarSystemObject&);
    void setCoordinateSystem(const CoordinateSystem&);
    void setVelocityReferenceFrame(const VelocityReferenceFrame&);
    void setZ(const double&);
    void setDopplerType(const DopplerType&);
    void setPatternType(const PatternType&);
    void setOtfType(const OTFType&);
    void setOtfLines(const int&);
    void setOtfRepetitions(const int&);
    void setSpectralType(const SpectralType&);
    void setSpectralPolarization(const SpectralPolarization&);
    void setSpectralBand(const Band&);
    // setters for things with units:
    void setSourceRA(ValueWithUnits*);
    void setSourceDec(ValueWithUnits*);
    void setRadialVelocity(ValueWithUnits*);
    void setParallax(ValueWithUnits*);
    void setPmRA(ValueWithUnits*);
    void setPmDec(ValueWithUnits*);
    void setPatternRAOffset(ValueWithUnits*);
    void setPatternDecOffset(ValueWithUnits*);
    void setPointSpacing(ValueWithUnits*);
    void setPatternAngle(ValueWithUnits*);
    void setPLength(ValueWithUnits*);
    void setQLength(ValueWithUnits*);
    void setOtfAmplitude(ValueWithUnits*);
    void setOtfDuration(ValueWithUnits*);
    void setOtfLineOffset(ValueWithUnits*);
    void setOtfAngle(ValueWithUnits*);
    void setSpectralLineRepresentativeFreq(ValueWithUnits*);
    void setSkyFrequency(ValueWithUnits*);
    void setRestFrequency(ValueWithUnits*);
    void setSpectralContinuumRepresentativeFreq(ValueWithUnits*);
    void setSpectralScanStartFreq(ValueWithUnits*);    
    void setSpectralScanEndFreq(ValueWithUnits*);



private:
    // regular members:
    QUuid m_uuid;
    QString m_name;
    QString m_description;
    SourceType m_sourceType;
    SolarSystemObject m_solarSystemObject;
    CoordinateSystem m_coordinateSystem;
    VelocityReferenceFrame m_velocityReferenceFrame;
    double m_z;
    DopplerType m_dopplerType;
    PatternType m_patternType;
    OTFType m_otfType;
    int m_otfLines;
    int m_otfRepetitions;
    SpectralType m_spectralType;
    SpectralPolarization m_spectralPolarization;
    Band m_spectralBand;
    
    // member of the special valuewithunits type:
    ValueWithUnits * m_sourceRA = 0;
    ValueWithUnits * m_sourceDec = 0;
    ValueWithUnits * m_radialVelocity = 0;
    ValueWithUnits * m_parallax = 0;
    ValueWithUnits * m_pmRA = 0;
    ValueWithUnits * m_pmDec = 0;
    ValueWithUnits * m_patternRAOffset = 0;
    ValueWithUnits * m_patternDecOffset = 0;
    ValueWithUnits * m_pointSpacing = 0;
    ValueWithUnits * m_patternAngle = 0;
    ValueWithUnits * m_pLength = 0;
    ValueWithUnits * m_qLength = 0;
    ValueWithUnits * m_otfAmplitude = 0;
    ValueWithUnits * m_otfDuration = 0;
    ValueWithUnits * m_otfLineOffset = 0;
    ValueWithUnits * m_otfAngle = 0;
    ValueWithUnits * m_spectralLineRepresentativeFreq = 0;
    ValueWithUnits * m_skyFrequency = 0;
    ValueWithUnits * m_restFrequency = 0;
    ValueWithUnits * m_spectralContinuumRepresentativeFreq = 0;
    ValueWithUnits * m_spectralScanStartFreq = 0;
    ValueWithUnits * m_spectralScanEndFreq = 0;
    

};

Q_DECLARE_METATYPE(Scan::SourceType);
Q_DECLARE_METATYPE(Scan::CoordinateSystem);
Q_DECLARE_METATYPE(Scan::VelocityReferenceFrame);
Q_DECLARE_METATYPE(Scan::DopplerType);
Q_DECLARE_METATYPE(Scan::PatternType);
Q_DECLARE_METATYPE(Scan::OTFType);
Q_DECLARE_METATYPE(Scan::SpectralType);
Q_DECLARE_METATYPE(Scan::SpectralPolarization);
Q_DECLARE_METATYPE(Scan::Band);
Q_DECLARE_METATYPE(Scan::SolarSystemObject);



namespace XMLStrings {
    static const QHash<Scan::SourceType, QString> sourceTypeNames = {
        { Scan::SourceTypeManual,      "manual"},
        { Scan::SourceTypeSolarSystem, "solarsystem"},
        { Scan::SourceTypeEphemeris,   "ephemeris"},
        { Scan::SourceTypeCatalog,     "catalog"}
    };

    static QHash<Scan::CoordinateSystem, QString> coordinateSystemNames = {
        { Scan::CoordinateSystemICRS,      "ICRS" },
        { Scan::CoordinateSystemFK5,       "FK5" },
        { Scan::CoordinateSystemGallactic, "gallactic" },
        { Scan::CoordinateSystemEcliptic,  "ecliptic" },
        { Scan::CoordinateSystemHorizon,   "horizon" },
        { Scan::CoordinateSystemAzEl,      "azel" }
    };
    static QHash<Scan::VelocityReferenceFrame, QString> velocityReferenceFrameNames = {
        { Scan::VelocityReferenceFrameBar,   "bar" },
        { Scan::VelocityReferenceFrameLsrk,  "lsrk" },
        { Scan::VelocityReferenceFrameTopo,  "topo" },
        { Scan::VelocityReferenceFrameHel,   "hel" }
    };
    static QHash<Scan::DopplerType, QString> dopplerTypeNames = {
        { Scan::DopplerTypeRadio,           "radio" },
        { Scan::DopplerTypeOptical,         "optical" },
        { Scan::DopplerTypeRelativistic,    "relativistic" }
    };
    static QHash<Scan::PatternType, QString> patternTypeNames = {
        { Scan::PatternTypeList,          "list" },
        { Scan::PatternTypeRectangular,   "rectangular" },
        { Scan::PatternTypeOTF,           "otf" },
        { Scan::PatternTypeHexagonal,     "hexagonal" }
    };
    static QHash<Scan::OTFType, QString> otfTypeNames = {
        { Scan::OTFTypeZigZag,      "zigzag" },
        { Scan::OTFTypeSameDir,     "samedir" }
    };
    static QHash<Scan::SpectralType, QString> spectralTypeNames = {
        { Scan::SpectralTypeLine,          "line" },
        { Scan::SpectralTypeContinuum,     "continuum" },
        { Scan::SpectralTypeScan,          "scan" }
    };
    static QHash<Scan::SpectralPolarization, QString> spectralPolarizationNames = {
        { Scan::SpectralPolarizationDual,   "dual" },
        { Scan::SpectralPolarizationXX,     "xx" },
        { Scan::SpectralPolarizationFull,   "full" }
    };
    static QHash<Scan::Band, QString> bandNames = {
        { Scan::Band3,   "3" },
        { Scan::Band4,   "4" },
        { Scan::Band5,   "5" },
        { Scan::Band6,   "6" },
        { Scan::Band7,   "7" },
        { Scan::Band9,   "9" },
        { Scan::Band10,  "10" }
    };
    static QHash<Scan::SolarSystemObject, QString> solarSystemObjectNames = {
        { Scan::Mercury,         "Mercury" },
        { Scan::Venus,           "Venus" },
        { Scan::Moon,            "Moon" },
        { Scan::Mars,            "Mars" },
        { Scan::Jupiter,         "Jupiter" },
        { Scan::Saturn,          "Saturn" },
        { Scan::Uranus,          "Uranus" },
        { Scan::Neptune,         "Neptune" },
        { Scan::Pluto,           "Pluto" },
        { Scan::Sun,             "Sun" },
        { Scan::Ganymede,        "Ganymede" },
        { Scan::Europa,          "Europa" },
        { Scan::Callisto,        "Callisto" },
        { Scan::Io,              "Io" },
        { Scan::Titan,           "Titan" },
        { Scan::Ceres,           "Ceres" },
        { Scan::Pallas,          "Pallas" },
        { Scan::Juno,            "Juno" },
        { Scan::Vesta,           "Vesta" }
    };

}


#endif // SCAN_H

