<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="67"/>
        <source>remove</source>
        <translation>verwijderen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="81"/>
        <source>add</source>
        <translation>Toevoegen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="110"/>
        <source>scan description</source>
        <translation type="unfinished">omschrijving</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="119"/>
        <source>&lt;description of science goal&gt;</source>
        <translation type="unfinished">&lt;beschrijving van science goal&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="126"/>
        <source>scan name: </source>
        <translation type="unfinished">scan naam: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>LLama Observing Tool</source>
        <translation type="unfinished">LLama Observatie Tool</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="150"/>
        <source>source</source>
        <translation>bron</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="169"/>
        <source>pm ra:</source>
        <translation type="unfinished">pm ra:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="180"/>
        <source>radio</source>
        <translation type="unfinished">radio</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <source>optical</source>
        <translation type="unfinished">optisch</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="190"/>
        <source>relativistic</source>
        <translation type="unfinished">relativistisch</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="198"/>
        <source>z:</source>
        <translation type="unfinished">z:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="208"/>
        <source>solar system object:</source>
        <translation type="unfinished">zonnestelstel lichaam:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>0.0000000000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="222"/>
        <location filename="../mainwindow.ui" line="323"/>
        <location filename="../mainwindow.ui" line="374"/>
        <location filename="../mainwindow.ui" line="381"/>
        <location filename="../mainwindow.ui" line="993"/>
        <location filename="../mainwindow.ui" line="1007"/>
        <location filename="../mainwindow.ui" line="1367"/>
        <location filename="../mainwindow.ui" line="1422"/>
        <location filename="../mainwindow.ui" line="1488"/>
        <location filename="../mainwindow.ui" line="1611"/>
        <location filename="../mainwindow.ui" line="1702"/>
        <location filename="../mainwindow.ui" line="1776"/>
        <source>0.0000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="232"/>
        <source>ephemeris object:</source>
        <translation type="unfinished">efemeer lichaam:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="242"/>
        <source>catalog object:</source>
        <translation type="unfinished">catalogus lichaam:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="249"/>
        <source>Dec:</source>
        <translation type="unfinished">Dec:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="260"/>
        <source>ICRS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="265"/>
        <source>FK5 J2000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="270"/>
        <source>gallactic</source>
        <translation type="unfinished">gallactisch</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="275"/>
        <source>ecliptic</source>
        <translation type="unfinished">ecliptisch</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="280"/>
        <source>horizon</source>
        <translation type="unfinished">horizon</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="285"/>
        <source>azel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <location filename="../mainwindow.ui" line="366"/>
        <location filename="../mainwindow.ui" line="978"/>
        <source>HH:MM:SS.sssss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="305"/>
        <source>degrees</source>
        <translation type="unfinished">graden</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="313"/>
        <source>velocity reference frame:</source>
        <translation type="unfinished">snelheidskader:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="330"/>
        <source>radial velocity (m/s):</source>
        <translation type="unfinished">radiale snelheid (m/s):</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="340"/>
        <location filename="../mainwindow.ui" line="986"/>
        <source>0.000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="347"/>
        <source>00:00:00.0000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="361"/>
        <location filename="../mainwindow.ui" line="973"/>
        <location filename="../mainwindow.ui" line="1050"/>
        <location filename="../mainwindow.ui" line="1074"/>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1117"/>
        <source>degree</source>
        <translation type="unfinished">graden</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="388"/>
        <source>parallax:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>&lt;no file&gt;</source>
        <translation type="unfinished">&lt;geen bestand&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="456"/>
        <source>open...</source>
        <translation type="unfinished">openen...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="466"/>
        <source>system:</source>
        <translation type="unfinished">systeem:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="476"/>
        <source>doppler type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="487"/>
        <source>bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="492"/>
        <source>lsrk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="497"/>
        <source>topo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="502"/>
        <source>hel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="511"/>
        <location filename="../mainwindow.ui" line="535"/>
        <source>mas/year</source>
        <translation type="unfinished">mas/jaar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="516"/>
        <location filename="../mainwindow.ui" line="540"/>
        <source>arcsec/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="521"/>
        <location filename="../mainwindow.ui" line="545"/>
        <source>arcmin/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="526"/>
        <location filename="../mainwindow.ui" line="550"/>
        <source>degree/s</source>
        <translation type="unfinished">graden/s</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="558"/>
        <source>RA:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="572"/>
        <source>Mercury</source>
        <translation type="unfinished">Mercurius</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="577"/>
        <source>Venus</source>
        <translation type="unfinished">Venus</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="582"/>
        <source>Moon</source>
        <translation type="unfinished">Maan</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="587"/>
        <source>Mars</source>
        <translation type="unfinished">Mars</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="592"/>
        <source>Jupiter</source>
        <translation type="unfinished">Jupiter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="597"/>
        <source>Saturn</source>
        <translation type="unfinished">Saturnus</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="602"/>
        <source>Uranus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="607"/>
        <source>Neptune</source>
        <translation type="unfinished">Neptunus</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="612"/>
        <source>Pluto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Sun</source>
        <translation type="unfinished">Zon</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Ganymede</source>
        <translation type="unfinished">Ganymedes</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="627"/>
        <source>Europa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="632"/>
        <source>Callisto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="637"/>
        <source>Io</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="642"/>
        <source>Titan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="647"/>
        <source>Ceres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="652"/>
        <source>Pallas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="657"/>
        <source>Juno</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="662"/>
        <source>Vesta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="671"/>
        <location filename="../mainwindow.ui" line="907"/>
        <location filename="../mainwindow.ui" line="1059"/>
        <location filename="../mainwindow.ui" line="1088"/>
        <location filename="../mainwindow.ui" line="1122"/>
        <location filename="../mainwindow.ui" line="1210"/>
        <location filename="../mainwindow.ui" line="1310"/>
        <location filename="../mainwindow.ui" line="1344"/>
        <source>mas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="676"/>
        <location filename="../mainwindow.ui" line="912"/>
        <location filename="../mainwindow.ui" line="1064"/>
        <location filename="../mainwindow.ui" line="1093"/>
        <location filename="../mainwindow.ui" line="1127"/>
        <location filename="../mainwindow.ui" line="1215"/>
        <location filename="../mainwindow.ui" line="1315"/>
        <location filename="../mainwindow.ui" line="1349"/>
        <source>arcsec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>pm dec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="702"/>
        <source>km/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="707"/>
        <source>m/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="715"/>
        <source>manual coordinates</source>
        <translation type="unfinished">handmatige coordinaten</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="726"/>
        <source>pattern</source>
        <translation type="unfinished">patroon</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="745"/>
        <source>coordinate offset list:</source>
        <translation type="unfinished">relatieve coordinaten:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="781"/>
        <source>New Row</source>
        <translation type="unfinished">Nieuwe rij</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="786"/>
        <source>RA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="791"/>
        <source>Dec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="796"/>
        <location filename="../mainwindow.ui" line="801"/>
        <source>0.00000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="811"/>
        <source>Add</source>
        <translation type="unfinished">Toevoegen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="818"/>
        <source>Remove</source>
        <translation type="unfinished">Verwijderen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="828"/>
        <source>Load from file</source>
        <translation type="unfinished">Importeren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="838"/>
        <source>Save to file</source>
        <translation type="unfinished">Exporteren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="868"/>
        <source>hexagonal grid?</source>
        <translation type="unfinished">hexagonaal raster?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="878"/>
        <source>grid options:</source>
        <translation type="unfinished">raster opties:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="884"/>
        <source>00:00:00.00000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="891"/>
        <source>q length:</source>
        <translation type="unfinished">q afstand:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="902"/>
        <source>fractions of beamsize</source>
        <translation type="unfinished">fractie van beamsize</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="917"/>
        <location filename="../mainwindow.ui" line="1069"/>
        <location filename="../mainwindow.ui" line="1098"/>
        <location filename="../mainwindow.ui" line="1132"/>
        <location filename="../mainwindow.ui" line="1220"/>
        <location filename="../mainwindow.ui" line="1320"/>
        <location filename="../mainwindow.ui" line="1354"/>
        <source>arcmin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="925"/>
        <source>p length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="935"/>
        <source>RA offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="945"/>
        <source>Dec offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="955"/>
        <source>0.51093</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="962"/>
        <source>angle:</source>
        <translation type="unfinished">hoek:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1000"/>
        <source>points in q direction:</source>
        <translation type="unfinished">punten in q richting:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1014"/>
        <source>points in p direction:</source>
        <translation type="unfinished">punten in p richting:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1021"/>
        <source>point spacing:</source>
        <translation type="unfinished">punt afstand:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1031"/>
        <source>0.000000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1045"/>
        <source>HH:MM:SS.ssssss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1079"/>
        <location filename="../mainwindow.ui" line="1108"/>
        <location filename="../mainwindow.ui" line="1137"/>
        <location filename="../mainwindow.ui" line="1225"/>
        <location filename="../mainwindow.ui" line="1330"/>
        <location filename="../mainwindow.ui" line="1359"/>
        <source>rad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1145"/>
        <location filename="../mainwindow.ui" line="1152"/>
        <source>NaN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1165"/>
        <source>on-the-fly mapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1172"/>
        <source>rectangular grid</source>
        <translation type="unfinished">rechthoekig raster</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1191"/>
        <source>list of coordinates</source>
        <translation type="unfinished">coordinatenlijst</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1198"/>
        <source>on-the-fly options:</source>
        <translation type="unfinished">on-the-fly opties:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1205"/>
        <location filename="../mainwindow.ui" line="1325"/>
        <location filename="../mainwindow.ui" line="1339"/>
        <source>deg</source>
        <translation type="unfinished">graden</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1233"/>
        <source>offset between lines:</source>
        <translation type="unfinished">lijnafstand:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1243"/>
        <source>angle: </source>
        <translation type="unfinished">hoek: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1253"/>
        <source>amplitude: </source>
        <translation type="unfinished">lijnlengte: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1276"/>
        <source>number of repetitions: </source>
        <translation type="unfinished">aantal herhalingen: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1286"/>
        <source>duration per line: </source>
        <translation type="unfinished">tijd per lijn: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1296"/>
        <source>number of lines: </source>
        <translation type="unfinished">aantal lijnen: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1375"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1380"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1390"/>
        <source>back and forth zig-zag</source>
        <translation type="unfinished">heen en weer zig-zag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1400"/>
        <source>all scans in same direction</source>
        <translation type="unfinished">alle scans in zelfde richting</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1443"/>
        <source>spectral</source>
        <translation type="unfinished">spectrum</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1449"/>
        <source>type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1472"/>
        <source>spectral line settings:</source>
        <translation type="unfinished">spectrumlijm instellingen:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1478"/>
        <location filename="../mainwindow.ui" line="1665"/>
        <source>representative frequency:</source>
        <translation type="unfinished">representatieve frequentie:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1495"/>
        <location filename="../mainwindow.ui" line="1709"/>
        <source>&lt;todo: per baseband settings here&gt;</source>
        <translation type="unfinished">&lt;todo: per baseband instellingen hier&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1505"/>
        <location filename="../mainwindow.ui" line="1509"/>
        <location filename="../mainwindow.ui" line="1628"/>
        <location filename="../mainwindow.ui" line="1632"/>
        <location filename="../mainwindow.ui" line="1675"/>
        <location filename="../mainwindow.ui" line="1679"/>
        <location filename="../mainwindow.ui" line="1831"/>
        <location filename="../mainwindow.ui" line="1905"/>
        <source>Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1514"/>
        <location filename="../mainwindow.ui" line="1637"/>
        <location filename="../mainwindow.ui" line="1684"/>
        <location filename="../mainwindow.ui" line="1826"/>
        <location filename="../mainwindow.ui" line="1900"/>
        <source>kHz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1519"/>
        <location filename="../mainwindow.ui" line="1642"/>
        <location filename="../mainwindow.ui" line="1689"/>
        <location filename="../mainwindow.ui" line="1821"/>
        <location filename="../mainwindow.ui" line="1895"/>
        <source>MHz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1524"/>
        <location filename="../mainwindow.ui" line="1647"/>
        <location filename="../mainwindow.ui" line="1694"/>
        <location filename="../mainwindow.ui" line="1769"/>
        <location filename="../mainwindow.ui" line="1816"/>
        <location filename="../mainwindow.ui" line="1890"/>
        <source>GHz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1536"/>
        <source>dual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1541"/>
        <source>xx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1546"/>
        <source>full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1561"/>
        <source>spectral line</source>
        <translation type="unfinished">spectrumlijn</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1566"/>
        <source>single continuum</source>
        <translation type="unfinished">los continuum</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1571"/>
        <source>spectral scan</source>
        <translation type="unfinished">spectrum scan</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1579"/>
        <location filename="../mainwindow.ui" line="1618"/>
        <source>rest frequency:</source>
        <translation type="unfinished">rust frequentie:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1585"/>
        <source>todo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1595"/>
        <source>polarization product:</source>
        <translation type="unfinished">polarizatie product:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1605"/>
        <source>single continuum settings:</source>
        <translation type="unfinished">continuum instellingen:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1655"/>
        <source>sky frequency:</source>
        <translation type="unfinished">hemel frequentie:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1720"/>
        <source>3 [84.0-116.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1725"/>
        <source>4 [125.0-163.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1730"/>
        <source>5 [163.0-211.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1735"/>
        <source>6 [211.0-275.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1740"/>
        <source>7 [275.0-373.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1745"/>
        <source>8 [385.0-500.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1750"/>
        <source>9 [602.0-720.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1755"/>
        <source>10 [787.0-950.0 GHz]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1783"/>
        <source>receiver band:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1796"/>
        <source>spectral scan settings:</source>
        <translation type="unfinished">spectrum scan instellingen:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1805"/>
        <source>requested range (rest): </source>
        <translation type="unfinished">gevraagde range (rustframe): </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1849"/>
        <source>requested start frequency (sky): </source>
        <translation type="unfinished">gevraagde start frequentie (hemel): </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1859"/>
        <source>achieved scan range (sky): </source>
        <translation type="unfinished">behaalde scan range (hemel): </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1869"/>
        <source>requested end frequency (sky): </source>
        <translation type="unfinished">gevraagde eind frequentie (hemel): </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1879"/>
        <source>bandwidth resolution (Hanning smoothed): </source>
        <translation type="unfinished">bandbreedte resolutie (Hanning smoothed): </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1921"/>
        <source>&lt;options not yet defined&gt;</source>
        <translation type="unfinished">&lt;opties nog niet gedefinieerd&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1933"/>
        <source>summary</source>
        <translation type="unfinished">samenvatting</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1939"/>
        <source>human readable summary of scan here</source>
        <translation type="unfinished">hier een human readable samenvatting</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1967"/>
        <source>area for logs, should disappear after debugging</source>
        <translation type="unfinished">logboek voor meldingen tijdens debuggen. Dit textveld zal later verdwijnen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1985"/>
        <source>File</source>
        <translation type="unfinished">Bestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1994"/>
        <source>Language</source>
        <translation type="unfinished">Taal</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2002"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2022"/>
        <source>English</source>
        <translation type="unfinished">Engels</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2033"/>
        <source>Dutch</source>
        <translation type="unfinished">Nederlands</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2044"/>
        <source>Spanish</source>
        <translation type="unfinished">Spaans</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2049"/>
        <source>Quit</source>
        <translation type="unfinished">Afsluiten</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2057"/>
        <source>Check for updates online</source>
        <translation type="unfinished">Online updates zoeken</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2062"/>
        <source>Open...</source>
        <translation type="unfinished">Openen...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2065"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2070"/>
        <source>Save</source>
        <translation type="unfinished">Opslaan</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2073"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>removing row %1</source>
        <translation type="unfinished">rij %1 wordt verwijderd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>new scan %1</source>
        <translation type="unfinished">nieuwe scan %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="213"/>
        <source>no item selected to remove!</source>
        <translation type="unfinished">geen item geselecteerd dat verwijderd kan worden!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="218"/>
        <source>scan item object not of Scan type!</source>
        <translation type="unfinished">scan item is niet van het juiste type!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="222"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Weet je het zeker?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="223"/>
        <source>This currently selected scan will be deleted and all info will be lost. This cannot be undone.</source>
        <translation type="unfinished">De geselecteerde scan zal worden verwijderd en alle geassocieerde informatie zal verloren zijn. Dit kan niet ongedaan gemaakt worden.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="240"/>
        <source>scan object not of type Scan!!</source>
        <translation type="unfinished">scan object is niet van het type Scan</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="253"/>
        <source>switching from %1 to %2</source>
        <translation type="unfinished">van %1 naar %2 geschakeld</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="257"/>
        <source>switching from %1 to nothing</source>
        <translation type="unfinished">%1 gedeselecteerd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="261"/>
        <source>switching from nothing to %1</source>
        <translation type="unfinished">overstappen naar %1, hiervoor niets geselecteerd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>Are you sure?
</source>
        <translation type="unfinished">Weet je het zeker?
</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="341"/>
        <source>new text: </source>
        <translation type="unfinished">nieuwe tekst: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="347"/>
        <source>qt translations for english successfully loaded</source>
        <translation type="unfinished">systeem vertaling voor Engels geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>english qt translations not found</source>
        <translation type="unfinished">systeem taal Engels niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="349"/>
        <source>qt translations for dutch successfully loaded</source>
        <translation type="unfinished">systeem vertaling voor Nederlands geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="350"/>
        <source>dutch qt translations not found</source>
        <translation type="unfinished">systeem taal Nederlands niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="351"/>
        <source>qt translations for spanish successfully loaded</source>
        <translation type="unfinished">systeem vertaling voor Spaans geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>spanish qt translations not found</source>
        <translation type="unfinished">systeem taal Spaans niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>app translations for english successfully loaded</source>
        <translation type="unfinished">applicatie vertaling voor Engels geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>english app translations not found</source>
        <translation type="unfinished">applicatie vertaling voor Engels niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>app translations for dutch successfully loaded</source>
        <translation type="unfinished">applicatie vertaling voor Nederlands geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>dutch app translations not found</source>
        <translation type="unfinished">applicatie vertaling voor Nederlands niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>app translations for spanish successfully loaded</source>
        <translation type="unfinished">applicatie vertaling voor Spaans geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>spanish app translations not found</source>
        <translation type="unfinished">applicatie vertaling voor Spaans niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="377"/>
        <location filename="../mainwindow.cpp" line="394"/>
        <location filename="../mainwindow.cpp" line="411"/>
        <source>%1 enabled</source>
        <translation type="unfinished">taal %1 geinstalleerd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="383"/>
        <location filename="../mainwindow.cpp" line="400"/>
        <location filename="../mainwindow.cpp" line="417"/>
        <source>%1 disabled</source>
        <translation type="unfinished">%1 verwijderd</translation>
    </message>
</context>
</TS>
