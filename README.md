# README #

### Installation ###
The program is stand-alone and requires no installation. Simply execute LLamaObservingTool.exe (windows), LLamaObservingTool-x86_64.AppImage (linux 64 bits) or LLamaObservingTool-i386.AppImage (linux 32 bits). You may need to set the executable bit with `chmod +x LLamaObservingTool-*.AppImage`
Note that there is not yet a mac version at this time. The AppImages *should* be self-contained. If you run into problems with dependencies please get in touch. 


### Compiling from source ###
If you *really must* compile from source you can do:
```bash
sudo apt-get install qt5-default qtbase5-dev
git clone <repo url>
cd llama-observingtool
qmake
make
./LLamaObservingTool
```

### Developing ###
The tool was developed using QtCreator. Steps to reproduce statically linked executables can be found on the wiki: wiki.llamaobservatory.org
The easiest way to create deployment images on linux is using linuxdeployqt and appimagetool.
