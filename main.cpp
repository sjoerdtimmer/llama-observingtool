#include "mainwindow.h"
#include <QApplication>
#include <QLibraryInfo>
#include <QTranslator>
#include <QString>

#include <iostream>

using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow w;

    w.show();

    return app.exec();
}
