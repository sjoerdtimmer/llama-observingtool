#ifndef UNITS_H
#define UNITS_H

#include <QHash>
#include <QVariant>
#include <QObject>
#include <QCoreApplication>
#include <QHash>
#include <QValidator>

#include <cmath>

// to store a hash of unittype to unitname a hash fuction on QVariants is needed.
// it is a bit ugly but this needs to be delared before the QHash import
uint qHash( const QVariant & var );




class HelloWorldValidator : public QValidator
{
public:
    QValidator::State validate(QString &input, int &pos) const;
    void fixup(QString &) const;
};

class MyDoubleValidator     : public QValidator
{
public:
    QValidator::State validate(QString &input, int &pos) const;
    void fixup(QString &) const;
};

class AngleHoursValidator : public QValidator
{
public:
    QValidator::State validate(QString &input, int &pos) const;
    void fixup(QString &) const;
};



AngleHoursValidator * getAngleHourValidatorInstance();
MyDoubleValidator * getDoubleValidatorInstance();

namespace Units {

    enum Angle {Hours, Degrees, Radians, ArcMin, ArcSec, MilliArcSec, Beamsizes};
    enum Speed {KilometersPerSec, MetersPerSec};
    enum AngularSpeed {MASPerYear, ArcSecPerSec, ArcMinPerSec, DegreesPerSec};
    enum Time {Seconds, Minutes};
    enum Frequency {Hz, kHz, MHz, GHz};


    static QHash<QVariant, QString> names = {
        {QVariant::fromValue(ArcMin), QCoreApplication::translate("units", "arcmin")},
        {QVariant::fromValue(ArcSec), QCoreApplication::translate("units", "arcsec")},
        {QVariant::fromValue(MilliArcSec), QCoreApplication::translate("units", "mas")},
        {QVariant::fromValue(Hours), QCoreApplication::translate("units", "HH:MM:SS.ssss")},
        {QVariant::fromValue(Degrees), QCoreApplication::translate("units", "degrees")},
        {QVariant::fromValue(Radians), QCoreApplication::translate("units", "radians")},
        {QVariant::fromValue(Beamsizes), QCoreApplication::translate("units", "fractions of beamsize")},

        {QVariant::fromValue(GHz), QCoreApplication::translate("units", "GHz")},
        {QVariant::fromValue(MHz), QCoreApplication::translate("units", "MHz")},
        {QVariant::fromValue(kHz), QCoreApplication::translate("units", "kHz")},
        {QVariant::fromValue(Hz), QCoreApplication::translate("units", "Hz")},

        {QVariant::fromValue(KilometersPerSec), QCoreApplication::translate("units", "km\\s")},
        {QVariant::fromValue(MetersPerSec), QCoreApplication::translate("units", "m\\s")},

        {QVariant::fromValue(Seconds), QCoreApplication::translate("units", "minutes")},
        {QVariant::fromValue(Minutes), QCoreApplication::translate("units", "seconds")},

        {QVariant::fromValue(ArcMinPerSec), QCoreApplication::translate("units", "arcmin/s")},
        {QVariant::fromValue(ArcSecPerSec), QCoreApplication::translate("units", "arcsec/s")},
        {QVariant::fromValue(DegreesPerSec), QCoreApplication::translate("units", "deg/s")},
        {QVariant::fromValue(MASPerYear), QCoreApplication::translate("units", "mas/year")}
    };

    // in xml unit attributes we must not translate so here are the "fixed" xml names of each unit
    static QHash<QVariant, QString> xmlNames = {
        {QVariant::fromValue(ArcMin), "arcmin"},
        {QVariant::fromValue(ArcSec), "arcsec"},
        {QVariant::fromValue(MilliArcSec), "mas"},
        {QVariant::fromValue(Hours), "hours"},
        {QVariant::fromValue(Degrees), "degrees"},
        {QVariant::fromValue(Radians), "radians"},
        {QVariant::fromValue(Beamsizes), "fractions of beamsize"},

        {QVariant::fromValue(GHz), "GHz"},
        {QVariant::fromValue(MHz), "MHz"},
        {QVariant::fromValue(kHz), "kHz"},
        {QVariant::fromValue(Hz), "Hz"},

        {QVariant::fromValue(KilometersPerSec), "km\\s"},
        {QVariant::fromValue(MetersPerSec), "m\\s"},

        {QVariant::fromValue(Seconds), "minutes"},
        {QVariant::fromValue(Minutes), "seconds"},

        {QVariant::fromValue(ArcMinPerSec), "arcmin/s"},
        {QVariant::fromValue(ArcSecPerSec), "arcsec/s"},
        {QVariant::fromValue(DegreesPerSec), "deg/s"},
        {QVariant::fromValue(MASPerYear), "mas/year"}
    };

    static QHash<QVariant, double> intrinsicvalues = {
        {QVariant::fromValue(ArcMin),      1.0/60.0      },
        {QVariant::fromValue(ArcSec),      1.0/3600.0    },
        {QVariant::fromValue(MilliArcSec), 1.0/3600000.0 },
        {QVariant::fromValue(Hours),       15.0 },
        {QVariant::fromValue(Degrees),     1.0 },
        {QVariant::fromValue(Radians),     180/M_PI },
        {QVariant::fromValue(Beamsizes),   1.0 }, // TODO find a way to do this

        {QVariant::fromValue(GHz),         1000000000.0 },
        {QVariant::fromValue(MHz),         1000000.0    },
        {QVariant::fromValue(kHz),         1000.0       },
        {QVariant::fromValue(Hz),          1.0          },

        {QVariant::fromValue(KilometersPerSec), 1000.0  },
        {QVariant::fromValue(MetersPerSec),     1.0     },

        {QVariant::fromValue(Seconds),     1.0          },
        {QVariant::fromValue(Minutes),     60.0         },

        {QVariant::fromValue(DegreesPerSec),  1.0        },
        {QVariant::fromValue(ArcMinPerSec),   1.0/60.0   },
        {QVariant::fromValue(ArcSecPerSec),   1.0/3600.0 },
        {QVariant::fromValue(MASPerYear),     1.0/3600000.0/(356.25*24.0*60.0*60.0) }

    };

    Q_ENUMS(Angle)
    Q_ENUMS(Speed)
    Q_ENUMS(AngularSpeed)
    Q_ENUMS(Time)
    Q_ENUMS(Frequency)

}

Q_DECLARE_METATYPE(Units::Angle);
Q_DECLARE_METATYPE(Units::Time);
Q_DECLARE_METATYPE(Units::Frequency);
Q_DECLARE_METATYPE(Units::Speed);
Q_DECLARE_METATYPE(Units::AngularSpeed);


#endif // UNITS_H
